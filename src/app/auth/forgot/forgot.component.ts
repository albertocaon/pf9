import { ApirezzemoloService } from '../../services/apirezzemolo.service';
import { Component, ElementRef, ViewChild, ChangeDetectorRef, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from "@angular/forms";
import { Router, NavigationExtras } from '@angular/router';
import { User } from '../../interfaces/user';
import { BehaviorSubject } from 'rxjs';



@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.sass']
})
export class ForgotComponent {
  
	form: FormGroup;
	loading: boolean = false;
	error: any;
	message: string;

	constructor(
		private _fb: FormBuilder, 
		public _apirezzemolo: ApirezzemoloService, 
		public router: Router, 
		private cdRef: ChangeDetectorRef
	) { 
		this.createForm();
		this.error = { email: '' };
	}

	@HostListener('document:click', ['$event']) 
		goToLogin($event){
		this.router.navigate(['welcome']);

	}


	createForm() {
	    this.form = this._fb.group({
			email: null
	    });
	}

	onSubmit() {
		console.info('Retrieving user info by email...');
		this._apirezzemolo.forgot(this.form.value.email).subscribe((data) => {
			console.log('data', data);
		  	if (data) {
		   		this.error = { email: '' };
		  		this.message = (data.message != '') ? 'Reset link was sent to your email. Check your inbox mail folder.' : data.message;
		  	} else {
		  		this.message = data.message;
		  	}  
		},
		err => {
			  this.error.email = 'Bad data';
			  this.message = "We can't find a user with that e-mail address.";
		});
	}
}

