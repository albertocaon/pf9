import { ApirezzemoloService } from '../../services/apirezzemolo.service';
import { Component, ElementRef, ViewChild, ChangeDetectorRef, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from "@angular/forms";
import { Router, NavigationExtras } from '@angular/router';
import { User } from '../../interfaces/user';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent {
  
  form: FormGroup;
  loading: boolean = false;
  error: any;
  message: string;

  constructor(
    private _fb: FormBuilder, 
    public _apirezzemolo: ApirezzemoloService, 
    public router: Router, private cdRef: ChangeDetectorRef
  ) { 
  	this.createForm();
    this.error = { email: '', password: '' };
  }

  onSubmit() {
    this.loading = true;
  	console.info('Trying to log in ...');
    this._apirezzemolo.login(this.form.value.email, this.form.value.password).subscribe((user) => {
      if (user.verified) {
        localStorage.setItem('user', JSON.stringify(user));
        localStorage.setItem('isLoggedIn', JSON.stringify(true));

        // Get the redirect URL from our auth service
        // If no redirect has been set, use the default
        let redirect = this._apirezzemolo.redirectUrl ? this._apirezzemolo.redirectUrl : '/';

        // Set our navigation extras object
        // that passes on our global query params and fragment
        let navigationExtras: NavigationExtras = {
          queryParamsHandling: 'preserve',
          preserveFragment: true
        };

        // Redirect the user
        this.router.navigate([redirect], navigationExtras);
      } else {
        this.message = 'Email needs to be verified, check your inbox mail folder';
      }
      
    },
    err => {
      localStorage.setItem('isLoggedIn', JSON.stringify(false));
      localStorage.setItem('user', JSON.stringify(null));
      this.error = err.error.errors;
      this.message = err.error.message;
    });
  }

  // signup() {
  //   // Set our navigation extras object
  //   // that passes on our global query params and fragment
  //   let navigationExtras: NavigationExtras = {
  //     queryParamsHandling: 'preserve',
  //     preserveFragment: true
  //   };

  //   // Redirect the user
  //   this.router.navigate(['/register'], navigationExtras);
  // }

  forgot() {
    // Redirect the user
    this.router.navigate(['/forgot']);
  }

  createForm() {
    this.form = this._fb.group({
      email: null,
      password: null,
    });
  }

}
