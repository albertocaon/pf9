import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { DishComponent } from './dish.component';
import { UploadImageComponent } from '../upload-image/upload-image.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatChipsModule } from '@angular/material/chips';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { Router, ActivatedRoute } from '@angular/router';

describe('DishComponent', () => {
  let component: DishComponent;
  let fixture: ComponentFixture<DishComponent>;
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatInputModule,
        MatSelectModule,
        MatFormFieldModule, 
        MatChipsModule,
        MatIconModule,
        MatAutocompleteModule,
        MatProgressSpinnerModule,
        MatProgressBarModule, 
        FormsModule, 
        ReactiveFormsModule,
        BrowserAnimationsModule,
        RouterModule
      ],
      declarations: [ 
        DishComponent,
        UploadImageComponent
      ],
      providers: [ 
        { provide: Router,      useValue: routerSpy },
        {
          provide: ActivatedRoute,
          useValue: {
              snapshot: {
                  paramMap: {
                      get(): string {
                          return '123';
                      },
                  },
              },
          },
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(async() => {
    fixture = TestBed.createComponent(DishComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    await fixture.whenStable();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form should exist', () => {
    expect(component.form).toBeTruthy();
  });

  it('form should have title property', () => {
    expect(component.form.value.title).toEqual('');
  });

  it('should render input elements', () => {
    const compiled = fixture.debugElement.nativeElement;
    const titleInput = compiled.querySelector('input[id="title"]');
    const descriptionInput = compiled.querySelector('input[id="description"]');
    const kindsSelectInput = compiled.querySelector('mat-select[id="kindsSelect"]');
    const chipListInput = compiled.querySelector('input[id="chipListInput"]');
    const recipeInput = compiled.querySelector('textarea[id="recipe"]');
    const imageUrlInput = compiled.querySelector('input[id="imageUrl"]');
    const imageInput = compiled.querySelector('input[id="image"]');

    expect(titleInput).toBeTruthy();
    expect(descriptionInput).toBeTruthy();
    expect(kindsSelectInput).toBeTruthy();
    expect(chipListInput).toBeTruthy();
    expect(recipeInput).toBeTruthy();
    expect(imageUrlInput).toBeFalsy();
    expect(imageInput).toBeFalsy();
  });

    it('check the length of kinds drop down', async () => {
    component.ngOnInit();
    const trigger = fixture.debugElement.query(By.css('.mat-select-trigger')).nativeElement;
    trigger.click();
    fixture.detectChanges();
    await fixture.whenStable().then(() => {
        const inquiryOptions = fixture.debugElement.queryAll(By.css('.mat-option-text'));
        expect(inquiryOptions.length).toEqual(component.kindNames.length);
    });
  });

  it('vegan option should have status according to variable', async () => {
    component.ngOnInit();
    component.kinds.vegan.checked = false;
    const trigger = fixture.debugElement.query(By.css('.mat-select-trigger')).nativeElement;
    trigger.click();
    fixture.detectChanges();
    await fixture.whenStable().then(async () => {
        const veganOption: HTMLElement = fixture.debugElement.query(By.css('#vegan')).nativeElement;
        veganOption.click();
        fixture.detectChanges();

        await fixture.whenStable().then(() => {
          expect(component.kinds.vegan.checked).toBeTruthy();
        })
    });
  });

  it('if vegan option is true, veggie and lactose free should be true too', async () => {
    component.ngOnInit();
    component.kinds.vegan.checked = false;
    const trigger = fixture.debugElement.query(By.css('.mat-select-trigger')).nativeElement;
    trigger.click();
    fixture.detectChanges();
    await fixture.whenStable().then(async () => {
        const veganOption: HTMLElement = fixture.debugElement.query(By.css('#vegan')).nativeElement;
        veganOption.click();
        fixture.detectChanges();

        await fixture.whenStable().then(() => {
          expect(component.kinds.vegan.checked).toBeTruthy();
          expect(component.kinds.veggie.checked).toBeTruthy();
          expect(component.kinds.lactoseFree.checked).toBeTruthy();
        })
    });
  });

  it('should test form validity', () => {
    const form = component.form;
    expect(form.valid).toBeFalsy();

    const titleInput = form.controls.title;
    const descriptionInput = form.controls.description;
    const veganInput = form.controls.vegan;
    const veggieInput = form.controls.veggie;
    const glutenFreeInput = form.controls.glutenFree;
    const lactoseFreeInput = form.controls.lactoseFree;
    const recipeInput = form.controls.recipe;

    titleInput.setValue('Wasabi');
    descriptionInput.setValue('Desc');
    veganInput.setValue(false);
    veggieInput.setValue(false);
    glutenFreeInput.setValue(false);
    lactoseFreeInput.setValue(false);
    recipeInput.setValue('Cook with love <3');

    expect(form.valid).toBeTruthy();
  });

  it('should test input validity', () => {
    const titleInput = component.form.controls.title;

    expect(titleInput.valid).toBeFalsy();

    titleInput.setValue('Wasabi');
    expect(titleInput.valid).toBeTruthy();
  });

  it('should get correct values on submit', () => {
    const titleInput = component.form.controls.title;
    titleInput.setValue('Wasabi');
    component.onSubmit();
    expect(component.dish.title).toEqual('Wasabi');
  });

  it('should get remove ingredient', () => {
    const ingredient = { name: 'wasabi'};
    component.ingredients.push(ingredient);
    fixture.detectChanges();
    expect(component.ingredients.indexOf(ingredient)).toEqual(0);
    const removeChip = fixture.debugElement.query(By.css('.mat-chip-remove')).nativeElement;
    removeChip.click();
    fixture.detectChanges();
    expect(component.ingredients.indexOf(ingredient)).toBeLessThan(0);
  });
});
