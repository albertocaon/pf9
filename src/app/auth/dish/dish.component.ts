import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Detail, newDish } from '../../interfaces/detail';
import { Kinds, Kind, defaultKinds } from '../../interfaces/kind';
import { Hashtag } from '../../interfaces/hashtag';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { ApirezzemoloService } from '../../services/apirezzemolo.service';
import { UploadService } from '../../services/upload.service';
import { PostService } from '../../services/post.service';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { environment } from '../../../environments/environment';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-dish',
  templateUrl: './dish.component.html',
  styleUrls: ['./dish.component.sass']
})
export class DishComponent implements OnInit {

	form: FormGroup;
	error = {
		title: '',
		description: '',
		recipe: ''

	};
	dish: Detail;
	kindNames: string[] = ['vegan', 'veggie', 'glutenFree', 'lactoseFree'];
	kinds: Kinds;
	checkedOptions = new FormControl();
	separatorKeysCodes = [COMMA];
  	ingredients = [];
  	matchingHashtags: Hashtag[];
	@ViewChild('hash') hash: ElementRef;
	term: string = '';
	visible: boolean = true;
	selectable: boolean = true;
	removable: boolean = true;
	fileFixedUrl: string = environment.apiUrl + 'file/';
	loading: boolean = true;
	message: string = '';
	ingredientsCtrl = new FormControl();
	filteredIngredients: Observable<string[]>;

	constructor(
		private _apirezzemolo: ApirezzemoloService, 
		private _upload: UploadService, 
		private activatedRoute: ActivatedRoute,
		private _post: PostService
	) {
		this.filteredIngredients = this.ingredientsCtrl.valueChanges.pipe(
        startWith(null),
        map((ingredient: string | null) => ingredient ? this._filter(ingredient) : this.ingredients.slice()));
	}
	
	ngOnInit() {
		this.dish = newDish;
		this.createForm();
		this.kinds = JSON.parse(JSON.stringify(defaultKinds));

		if (this.activatedRoute.snapshot.paramMap.get('idPost')) {
	        const idPost = parseInt(this.activatedRoute.snapshot.paramMap.get('idPost'));
	        this._apirezzemolo.getPost(idPost).subscribe((post: Detail) => {
	        	this.dish = post;
	        	this.createForm();
	        	this.setKind(this.dish);
	        	this.setIngredients(this.dish);
	        	this.loading = false;
	        });
      	} else {
			this.loading = false;
      	}

	    this._upload.newDishImageUploaded.subscribe((uploadedData) => {
	    	this.dish.imageUrl = uploadedData.imageUrl;
	    	this.dish.image = uploadedData.image;
	    });	
	}

	private setKind(dish: Detail) {
		let checked: string[] = [];
		for (let kn of this.kindNames) {
			if (this.dish[kn] == 1) {
				checked.push(kn);
			} 
		}
		this.checkedOptions = new FormControl(checked);
	}

	private setIngredients(dish: Detail) {
		this.ingredients = JSON.parse(dish.ingredients);
	}

	private createForm() {
		this.form = new FormGroup({
	      	'title': new FormControl(this.dish.title, [
	      		Validators.required,
	      		Validators.maxLength(63)
	      	]),
	      	'description': new FormControl(this.dish.description, [
	      		Validators.required,
	      		Validators.maxLength(255)
	      	]),
	      	'ingredients': new FormControl(this.dish.ingredients),
	      	'vegan': new FormControl(this.dish.vegan, [
	      		Validators.required
	      	]),
	      	'veggie': new FormControl(this.dish.veggie, [
	      		Validators.required
	      	]),
	      	'glutenFree': new FormControl(this.dish.glutenFree, [
	      		Validators.required
	      	]),
      		'lactoseFree': new FormControl(this.dish.lactoseFree, [
	      		Validators.required
	      	]),
			'recipe': new FormControl(this.dish.recipe)
	    });
	}

	onSubmit() {
		this.dish = {
			id: this.dish.id,
			idUser: this.dish.idUser,
			title: this.form.value.title ? this.form.value.title : this.dish.title,
			description: this.form.value.description ? this.form.value.description : this.dish.description,
			ingredients: JSON.stringify(this.ingredients),
			vegan: this.kinds.vegan.checked,
			veggie: this.kinds.veggie.checked,
			glutenFree: this.kinds.glutenFree.checked,
			lactoseFree: this.kinds.lactoseFree.checked,
			recipe: this.form.value.recipe ? this.form.value.recipe : this.dish.recipe,
			imageUrl: this.dish.imageUrl,
			image: this.dish.image
		}
        if (this.dish.id === null) {
        	this.message = 'Creating new post...';
	        this._apirezzemolo.createPost(this.dish);
        } else {
        	this.message = 'Updating post...';
        	this._apirezzemolo.updatePost(this.dish);
        }
    }

    coherence(selected) {
		let checked: string[] = this.checkedOptions.value;
		this.kinds[selected].checked = !this.kinds[selected].checked;
		const isChecked: boolean = this.kinds[selected].checked;
	 	checked = this.triggerConsequentTruths(selected, checked, isChecked);
	 	checked = this.triggerConsequentFalses(selected, checked, isChecked);
	 	this.checkedOptions = new FormControl(checked);
	 	this.dish.veggie = checked.includes('veggie');
	 	this.dish.vegan = checked.includes('vegan'); 
	 	this.dish.glutenFree= checked.includes('glutenFree'); 
	 	this.dish.lactoseFree = checked.includes('lactoseFree'); 

	}

	triggerConsequentTruths(selected, checked, isChecked) {
		for (let trueTriggered of this.kinds[selected].conditionalTriggers[isChecked ? 'isChecked' : 'isNotChecked'].truths) {
	 		this.kinds[trueTriggered].checked = true;
	 		checked[trueTriggered] ? '' : checked.push(trueTriggered);
	 		checked = [...new Set(checked)];
	 	}

	 	return checked;
	}

	triggerConsequentFalses(selected, checked, isChecked) {
		for (let falseTriggered of this.kinds[selected].conditionalTriggers[isChecked ? 'isChecked' : 'isNotChecked'].falses) {
	 		this.kinds[falseTriggered].checked = false;
	 		checked = checked.filter((kind: string) => kind !== falseTriggered);
	 		checked = [...new Set(checked)];
	 	}

	 	return checked;
	}

	add(event: MatChipInputEvent): void {
	    let input = event.input;
	    let value = event.value;

	    // Add our ingredient
	    if ((value || '').trim()) {
	      this.ingredients.push({ name: value.trim() });
	    }

	    // Reset the input value
	    if (input) {
	      input.value = '';
	    }
	}

	selected(event: MatAutocompleteSelectedEvent): void {
	    const chipInputEvent: MatChipInputEvent = {
	    	input: this.hash.nativeElement,
	    	value: '#' + event.option.value.name
	    }
	    this.add(chipInputEvent);
	}

	searchHashtags(term: string) {
	    if (term.substring(0, 1) === '#' && this.term != term) {
			this.term = term;
			this._apirezzemolo.getMatchingHashtags(term).subscribe( (hashtags) => {
				this.matchingHashtags = hashtags;
			});
	    }
	}

	remove(ingredient: any): void {
	    let index = this.ingredients.indexOf(ingredient);

	    if (index >= 0) {
			this.ingredients.splice(index, 1);
	    }
	} 

	private _filter(value: string): string[] {
		const filterValue = value.toLowerCase();

		return this.ingredients.filter(ingredient => ingredient.toLowerCase().indexOf(filterValue) === 0);
	} 
}