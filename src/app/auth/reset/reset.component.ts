import { ApirezzemoloService } from '../../services/apirezzemolo.service';
import { Component, ElementRef, ViewChild, ChangeDetectorRef, Input, Output, EventEmitter, OnInit, HostListener } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from "@angular/forms";
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { User } from '../../interfaces/user';
import { BehaviorSubject } from 'rxjs';



@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.sass']
})
export class ResetComponent implements OnInit{
  
	form: FormGroup;
	loading: boolean = false;
	error: any;
	message: string;
	token: string;
	email: string;
	showForm: boolean;
	resetDone: boolean;

	constructor(
		private activatedRoute: ActivatedRoute, 
		private _fb: FormBuilder, 
		public _apirezzemolo: ApirezzemoloService, 
		public router: Router, 
		private cdRef: ChangeDetectorRef
	) {
		this.showForm = false;
		this.resetDone = false;
	}

	@HostListener('document:click', ['$event']) 
		goToLogin($event){
		this.router.navigate(['welcome']);

	}

	createForm() {
	    this.form = this._fb.group({
			email: this.email,
			password: null,
			password_confirmation: null,
			token: this.token,
	    });
	}

	ngOnInit() {
		this.token = this.activatedRoute.snapshot.paramMap.get('token');
		this._apirezzemolo.getUserByRememberToken(this.token).subscribe((user) => {
		  	if (user) {
		  		// Redirect the user
    			this.email = user.email;
    			this.createForm();
				this.error = { email: '' };
				this.showForm = true;
		  	} else {
		    	this.message = 'An error occurred';
		  	}
		  
		},
		err => {
			console.log('err', err);
			  this.error = err.error.errors;
			  this.message = err.error.message;
		});

		
	}

	onSubmit() {
		this._apirezzemolo.reset(this.form.value.email, this.form.value.password, this.form.value.password_confirmation, this.form.value.token).subscribe((success) => {
		  	if (success) {
		  		this.message = success.message;
    			this.resetDone = true;
		  	} else {
		    	this.message = 'An error occurred';
		  	}
		  
		},
		err => {
			console.log('err', err);
			  this.error = err.error.errors;
			  this.message = err.message;
		});
	}
}