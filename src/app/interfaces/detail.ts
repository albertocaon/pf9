export interface Detail {
    "id": number;
    "idUser": number;
    "vegan": boolean;
    "veggie": boolean;
    "glutenFree": boolean;
    "lactoseFree": boolean;
    "image": number;
    "imageUrl": string;
    "rating"?: number;
    "title": string;
    "description": string;
    "ingredients"?: string;
    "recipe"?: string;
    "views"?: number;
    "created_at"?: string;
}

export interface Ingredient {
    "name": string;
}

export const newDish:Detail = {
    id: null,
    idUser: 74,
    vegan: true,
    veggie: true,
    glutenFree: false,
    lactoseFree: true,
    image: null,
    imageUrl: '',
    rating: null,
    title: '',
    description: '',
    ingredients: '',
    recipe: '',
    views: 0,
}