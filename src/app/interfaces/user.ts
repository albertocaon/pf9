export interface User {
	"id": number;
	"username": string;
	"email": string;
	"mobile": string;
	"siteUrl": string;
	"showMail": boolean;
	"showMobile": boolean;
	"showSiteUrl": boolean;
	"profileImage": number;
	"profileImageUrl": string;
	"role"?: string;
	"api_token"?: string;
	"verified"?: boolean;
	"postCount"?: number;
	"views"?: number;
}

export interface Authors {
	[key: number]: User;
}

export const PF: User = {
	id: 74,
	username: 'PF',
	email: 'prezzemolofresco@gmail.com',
	mobile: '(+34) 681 148 750',
	siteUrl: 'https://prezzemolofresco.com',
	showMail: true,
	showMobile: false,
	showSiteUrl: true,
	profileImage: 374,
	profileImageUrl: 'php6fDRh9.png',
	role: 'User'
}
