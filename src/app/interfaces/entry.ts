export interface Entry {
	"id": number;
	"filename": string;
	"mime": string;
	"original_filename": string;
}
