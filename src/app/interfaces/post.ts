export interface Post {
    "id": number;
    "idUser": number;
    "vegan": boolean;
    "veggie": boolean;
    "glutenFree": boolean;
    "lactoseFree": boolean;
    "image": number;
    "imageUrl": string;
    "title": string;
    "description": string;
    "views": number;
}

export interface Posts {
    [key: number]: Post;
}