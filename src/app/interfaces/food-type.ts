export interface FoodType {
	"vegan": boolean;
	"veggie": boolean;
	"glutenFree": boolean;
	"lactoseFree": boolean;
}

export const defaultFoodType: FoodType = {
	"vegan": true,
	"veggie": true,
	"glutenFree": false,
	"lactoseFree": true,
}