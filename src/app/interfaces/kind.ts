export interface Kind {
	"name": string;
	"checked": boolean;
	"desc": string;
	"conditionalTriggers": ConditionalTriggers;
}

export interface Kinds {
	[key: string]: Kind;
}

export interface ConditionalTriggers {
	"isChecked": Triggers;
	"isNotChecked": Triggers;
}

export interface Triggers {
	"truths": string[];
	"falses": string[];
}

export const defaultKinds: Kinds = {
			vegan: {
				name: 'vegan',
				checked: true,
				desc: 'Nothing animal',
				conditionalTriggers: {
					isChecked: {
						truths: ['veggie', 'lactoseFree'],
						falses: [],
					},
					isNotChecked: {
						truths: [],
						falses: []
					}
				} 
			},
			veggie: {
				name: 'veggie',
				checked: true,
				desc: 'No animals, only derivates',
				conditionalTriggers: {
					isChecked: {
						truths: [],
						falses: [],
					},
					isNotChecked: {
						truths: [],
						falses: ['vegan']
					}
				} 
			},
			glutenFree: {
				name: 'glutenFree',
				checked: false,
				desc: 'No gluten',
				conditionalTriggers: {
					isChecked: {
						truths: [],
						falses: [],
					},
					isNotChecked: {
						truths: [],
						falses: []
					}
				} 
			},
			lactoseFree: {
				name: 'lactoseFree',
				checked: true,
				desc: 'No lactose',
				conditionalTriggers: {
					isChecked: {
						truths: [],
						falses: [],
					},
					isNotChecked: {
						truths: [],
						falses: ['vegan']
					}
				} 
			}
		};
