import { FoodType } from './food-type';
import { Post } from './post';
export interface SearchPostsParams {
	"pageNumber": number;
	"pageSize": number;
	"sortOrder": string;
	"filters"?: string;
	"foodType": FoodType;
}

export interface Searches {
	[key: number]: SearchResult;
}

export interface SearchResult {
	"searchPostsParams": SearchPostsParams;
	"result": Post[];
}
