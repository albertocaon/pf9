export const navLinks: NavLink[] = [
	{
		path: 'home',
		label: 'Recetas',
		icon: 'restaurant_menu'
	},
	{
		path: 'hashtags',
		label: 'Hashtags',
		icon: 'forward'
	},
	{
        path: 'info',
        label: 'Quién soy',
        icon: 'sentiment_very_satisfied'
	},  
	{
		path: 'login',
		label: 'Admin',
		icon: 'lock'
	}
]

export const adminLinks: NavLink[] = [
	{
        path: 'dish',
        label: 'New Dish',
        icon: 'add'
      },
      {
        path: 'top',
        label: 'Top 20',
        icon: 'star'
      },
      // {
      //   path: 'settings',
      //   label: 'Settings',
      //   icon: 'settings'
      // },
      {
        path: '/',
        label: 'Log out',
        icon: 'clear'
      }
]

export interface NavLink {
	"path": string;
	"label": string;
	"icon": string;
}