export interface UserAbstract {
	"id": number;
	"username": string;
	"profileImageUrl": string;
	"views": number;
}
