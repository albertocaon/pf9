import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'matchUrl'
})
export class MatchUrlPipe implements PipeTransform {

	transform(value: string, arg?: any): any {
		let result = '';
		let exp = /(https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=()]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_\+.~#?&//=()]*))/g;
		return value.replace(exp, "<a class='typedLink' target='_blank' href='$1'>$1</a>");
	}

}
