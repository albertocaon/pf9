import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { DetailComponent } from './components/home/post/detail/detail.component';
import { TopComponent } from './components/home/top/top.component';
import { HashtagsComponent } from './components/hashtags/hashtags.component';
import { DishComponent } from './auth/dish/dish.component';
import { LoginComponent } from './auth/login/login.component';
import { ForgotComponent } from './auth/forgot/forgot.component';
import { ResetComponent } from './auth/reset/reset.component';
import { SettingsComponent } from './auth/settings/settings.component';
import { InfoComponent } from './components/info/info.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AuthGuard } from './services/auth-guard.service';

const routes: Routes = [
  {
      path: 'dish',
      component: DishComponent,
      canLoad: [AuthGuard]
  },
  {
      path: 'edit/:idPost',
      component: DishComponent,
      canLoad: [AuthGuard]
  },
  {
    path: 'settings',
    component: SettingsComponent,
    canLoad: [AuthGuard]
  },
  {
    path: 'top',
    component: TopComponent,
    canLoad: [AuthGuard]
  },
  {
    path: 'forgot',
    component: ForgotComponent,
    canLoad: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'info',
    component: InfoComponent
  },
  {
    path: 'reset/:token',
    component: ResetComponent
  },
  {
    path: 'detail/:idPost/:title',
    component: DetailComponent, data: { animation: 'DetailPage' }
  },
	{
    path: 'detail/:idPost',
    component: DetailComponent, data: { animation: 'DetailPage' }
  },
  {
    path: 'hashtag/:hashtag',
    component: HomeComponent, data: { animation: 'HomePage' }
  },
  {
    path: 'hashtags',
    component: HashtagsComponent
  },
  { 
    path: 'home', 
    component: HomeComponent, data: { animation: 'HomePage' } 
  },
  {
    path: '404', component: PageNotFoundComponent
  },
  { path: '', redirectTo: 'home', pathMatch: 'full'},
	{
		path: '**', redirectTo: '404'
	}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
