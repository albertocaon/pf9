import { Component, OnInit, HostListener, ChangeDetectorRef, ViewRef } from '@angular/core';
import { SearchService } from '../../services/search.service';
import { PostService } from '../../services/post.service';
import { PwaService } from '../../services/pwa.service';
import { Post } from '../../interfaces/post';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  showHashtagName: boolean = false;
  count: any = 0;

  constructor(
    public _search: SearchService,
    private _post: PostService,
    public router: Router,
    private cdr: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    public _pwa: PwaService,
  ) { }

  ngOnInit() {
    this.evaluateFilters();
    this.getPostsCount();

    this._search.searchPostsParamsChanged.subscribe(() => {
      this.getPostsCount();
    });
  }

  getPostsCount() {
    this._post.getPostsCount(this._search.searchPostsParams).subscribe((count) => {
      this.count = count;
    }, err => console.error('Error', err));
  }

  evaluateFilters() {
    this._search.hashtag.name = this.activatedRoute.snapshot.paramMap.get('hashtag') ? this.activatedRoute.snapshot.paramMap.get('hashtag') : '';
    if (this._search.hashtag.name && this._search.hashtag.name !== '') {
      this.showHashtagName = true;
      this._search.searchPostsParams.filters = '#' + this._search.hashtag.name;
      this._search.searchPostsParamsChanged.emit(true);
    } else {
      this.showHashtagName = false;
    }

    setTimeout(() => {
      if (this.cdr && !(this.cdr as ViewRef).destroyed) {
        this.cdr.detectChanges();
      }
    });
  }

  installPwa(): void {
    this._pwa.promptEvent.prompt();
  }
}
