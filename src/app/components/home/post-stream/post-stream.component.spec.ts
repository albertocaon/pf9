import { ApirezzemoloService } from '../../../services/apirezzemolo.service';
import { SearchService } from '../../../services/search.service';
import { GoogleAnalyticsService } from '../../../services/google-analytics.service';
import { PostStreamComponent } from './post-stream.component';
import { PostComponent } from '../post/post.component';
import {
  CdkVirtualForOf,
  CdkVirtualScrollViewport,
  ScrollDispatcher,
  ScrollingModule
} from '@angular/cdk/scrolling';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { PostHeaderComponent } from '../post/post-header/post-header.component';
import { PostThumbnailComponent } from '../post/post-thumbnail/post-thumbnail.component';
import { PostFooterComponent } from '../post/post-footer/post-footer.component';
import { DetailComponent } from '../post/detail/detail.component';
import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { MatchUrlPipe } from '../../../pipes/match-url.pipe';
import { MomentModule } from 'ngx-moment';
import { Router } from '@angular/router';
import { DateComponent } from '../post/detail/date/date.component';
import { IngredientsComponent } from '../post/detail/ingredients/ingredients.component';
import { KindDescriptionComponent } from '../post/detail/kind-description/kind-description.component';
import { RecipeComponent } from '../post/detail/recipe/recipe.component';
import { ViewsComponent } from '../post/detail/views/views.component';
import { AdminControlsComponent } from '../post/detail/admin-controls/admin-controls.component';

describe('PostStreamComponent', () => {
  let component: PostStreamComponent;
  let fixture: ComponentFixture<PostStreamComponent>;
  let viewport: CdkVirtualScrollViewport;
  let injector: TestBed;
  let _apirezzemolo: ApirezzemoloService;
  let _ga: GoogleAnalyticsService;
  let _search: SearchService;
  let httpMock: HttpTestingController;
  let httpClientSpy: { get: jasmine.Spy };
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ScrollingModule,
        HttpClientTestingModule,
        RouterModule,
        MomentModule
      ],
        declarations: [
        PostStreamComponent,
        PostComponent,
        PostHeaderComponent,
        PostThumbnailComponent,
        PostFooterComponent,
        DetailComponent,
        MatchUrlPipe,
        DateComponent,
        IngredientsComponent,
        KindDescriptionComponent,
        RecipeComponent,
        ViewsComponent,
        AdminControlsComponent
      ],
      providers: [
        ApirezzemoloService, 
        GoogleAnalyticsService,
        SearchService,
        { provide: Router, useValue: routerSpy }
      ],
    })
    .compileComponents();

    injector = getTestBed();
    _apirezzemolo = injector.get(ApirezzemoloService);
    httpMock = injector.get(HttpTestingController);
  }));

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    _search = new SearchService(<any> routerSpy, _ga);
    _apirezzemolo = new ApirezzemoloService(<any> httpClientSpy, _search, <any> routerSpy);
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(PostStreamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have post chunks', () => {
    expect(component.chunks).toBeTruthy();
  });

  it('chunks length should be 0 if no posts', () => {
    component.dataSource = null;
    expect(Object.keys(component.chunks).length).toEqual(0);
  });
});
