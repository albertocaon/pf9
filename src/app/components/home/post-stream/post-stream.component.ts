import { Component, OnInit, ChangeDetectorRef, ViewRef, ViewChild } from '@angular/core';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Post, Posts } from '../../../interfaces/post';
import { SearchService } from '../../../services/search.service';
import { BehaviorSubject, Subscription, Observable } from 'rxjs';
import { PostService } from '../../../services/post.service';
import { HostListener } from "@angular/core";
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';

@Component({
  selector: 'app-post-stream',
  templateUrl: './post-stream.component.html',
  styleUrls: ['./post-stream.component.sass']
})
export class PostStreamComponent implements OnInit {
  dataSource: PostDataSource;
  chunks: Posts = {};
  itemSize: number = 0;
  // Declare height and width variables
  scrHeight:any;
  scrWidth:any;

  @ViewChild(CdkVirtualScrollViewport, {static: true}) viewport: CdkVirtualScrollViewport;

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
        this.scrHeight = window.innerHeight;
        this.scrWidth = window.innerWidth;
        this.itemSize = this.scrWidth < 1024 ? Math.round(this.scrWidth * 0.94) * 3 : 352;
  }

  constructor(
    private _post: PostService,
  	public _search: SearchService,
    private cdr: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.getScreenSize();
    this._search.scrollPostStreamToTop.subscribe(() => {
      if (this.viewport) {
        this.viewport.scrollToIndex(0);
      }
    });
    this.dataSource = new PostDataSource(this._post, this._search, this.cdr);
    this._search.searchPostsParamsChanged.subscribe(() => {
      this.dataSource = new PostDataSource(this._post, this._search, this.cdr);
      setTimeout(() => {
        if (this.cdr && !(this.cdr as ViewRef).destroyed) {
          this.cdr.detectChanges();
        }
      });
    });
  }
}

export class PostDataSource extends DataSource<Posts | undefined> {
  cachedChunks = Array.from<Posts>({ length: 0 });
  private dataStream = new BehaviorSubject<(Posts | undefined)[]>(this.cachedChunks);
  private subscription = new Subscription();

  constructor(
    private _post: PostService,
    private _search: SearchService, 
    private cdr: ChangeDetectorRef
  ) {
    super();

    // Start with some data.
    this._fetchPostPage();
  }

  connect(collectionViewer: CollectionViewer): Observable<(Posts | undefined)[] | ReadonlyArray<Posts | undefined>> {
    this.subscription.add(collectionViewer.viewChange.subscribe(range => {
      const currentPage = this._getPageForIndex(range.end);

      if (currentPage > this._search.searchPostsParams.pageNumber) {
        this._search.searchPostsParams.pageNumber = currentPage;
        this.getPosts();
      }
    }));

    return this.dataStream;
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.subscription.unsubscribe();
  }

  private _fetchPostPage(): void {
    this.getPosts();
  }

  private _getPageForIndex(i: number): number {
    return Math.floor(i * 3 / this._search.searchPostsParams.pageSize);
  }


  getPosts() {
    this._post.getPosts(this._search.searchPostsParams).subscribe((posts: Post[]) => {
      const chunks: Posts[] = this.chunk(posts, 3);
      this.cachedChunks = this.cachedChunks.concat(chunks);
      this.dataStream.next(this.cachedChunks);
      setTimeout(() => {
        if (this.cdr && !(this.cdr as ViewRef).destroyed) {
          this.cdr.detectChanges();
        }
      });
    }, err => console.error('Error', err));
  }

  chunk(array, size): any {
    const chunked_arr = [];
    let index = 0;
    while (index < array.length) {
      chunked_arr.push(array.slice(index, size + index));
      index += size;
    }
    return chunked_arr;
  }

}
