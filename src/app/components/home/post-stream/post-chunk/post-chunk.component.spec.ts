import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostChunkComponent } from './post-chunk.component';

describe('PostChunkComponent', () => {
  let component: PostChunkComponent;
  let fixture: ComponentFixture<PostChunkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostChunkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostChunkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
