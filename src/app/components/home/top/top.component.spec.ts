import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TopComponent } from './top.component';
import { ApirezzemoloService } from '../../../services/apirezzemolo.service';
import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { MomentModule } from 'ngx-moment';
import { Router } from '@angular/router';
import { PF } from '../../../interfaces/user';

describe('TopComponent', () => {
  let component: TopComponent;
  let fixture: ComponentFixture<TopComponent>;
  let httpMock: HttpTestingController;
  let httpClientSpy: { get: jasmine.Spy };
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterModule,
        MomentModule
      ],
      declarations: [ 
        TopComponent
      ],
      providers: [
        ApirezzemoloService, 
        { provide: Router, useValue: routerSpy }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopComponent);
    component = fixture.componentInstance;
    localStorage.setItem('user', JSON.stringify(PF));
    localStorage.setItem('isLoggedIn', JSON.stringify(true));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have howMany constant', () => {
    expect(component.howMany).toBeDefined()
  });

  it('should have howMany set to 20', () => {
    expect(component.howMany).toEqual(27);
  });

  it('should have posts defined', () => {
    expect(component.posts).toBeDefined()
  });
});
