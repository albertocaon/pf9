import { Component, OnInit } from '@angular/core';
import { ApirezzemoloService } from '../../../services/apirezzemolo.service';
import { Post, Posts } from '../../../interfaces/post';

@Component({
  selector: 'pf9-top',
  templateUrl: './top.component.html',
  styleUrls: ['./top.component.sass']
})
export class TopComponent implements OnInit {

  howMany: number = 27;
  posts: Post[] = [];

  constructor(private _apirezzemolo: ApirezzemoloService) { }

  ngOnInit(): void {
  	this._apirezzemolo.getTop(this.howMany).subscribe((posts: Post[]) => {
  		this.posts = posts;
  	})
  }

}
