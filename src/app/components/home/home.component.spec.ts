import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpErrorResponse } from '@angular/common/http';
import { ApirezzemoloService } from '../../services/apirezzemolo.service';
import { GoogleAnalyticsService } from '../../services/google-analytics.service';
import { SearchService } from '../../services/search.service';
import { PwaService } from '../../services/pwa.service';
import { SwUpdate } from '@angular/service-worker';
import { Post } from '../../interfaces/post';
import { FoodType } from '../../interfaces/food-type';
import { HomeComponent } from './home.component';
import { PostComponent } from './post/post.component';
import { PostHeaderComponent } from './post/post-header/post-header.component';
import { PostThumbnailComponent } from './post/post-thumbnail/post-thumbnail.component';
import { PostFooterComponent } from './post/post-footer/post-footer.component';
import { DetailComponent } from './post/detail/detail.component';
import { PostStreamComponent } from './post-stream/post-stream.component';
import { environment } from '../../../environments/environment';
import { dummyPosts } from '../../mocks/posts.mock';
import { defer } from 'rxjs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  CdkVirtualForOf,
  CdkVirtualScrollViewport,
  ScrollDispatcher,
  ScrollingModule
} from '@angular/cdk/scrolling';
import { By } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { MatchUrlPipe } from '../../pipes/match-url.pipe';
import { MomentModule } from 'ngx-moment';
import { Router, ActivatedRoute } from '@angular/router';
import { DateComponent } from './post/detail/date/date.component';
import { IngredientsComponent } from './post/detail/ingredients/ingredients.component';
import { KindDescriptionComponent } from './post/detail/kind-description/kind-description.component';
import { RecipeComponent } from './post/detail/recipe/recipe.component';
import { ViewsComponent } from './post/detail/views/views.component';
import { AdminControlsComponent } from './post/detail/admin-controls/admin-controls.component';
import { ServiceWorkerModule } from '@angular/service-worker';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let injector: TestBed;
  let _apirezzemolo: ApirezzemoloService;
  let _search: SearchService;
  let _ga: GoogleAnalyticsService;
  let httpMock: HttpTestingController;
  let httpClientSpy: { get: jasmine.Spy };
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
       imports: [
        HttpClientTestingModule,
        MatInputModule,
        MatSelectModule,
        MatFormFieldModule, 
        FormsModule, 
        ReactiveFormsModule,
        BrowserAnimationsModule,
        ScrollingModule,
        RouterModule,
        MomentModule,
        ServiceWorkerModule.register('', {enabled: false})
      ],
      providers: [
        ApirezzemoloService, 
        SearchService, 
        GoogleAnalyticsService,
        PwaService,
        SwUpdate,
        {
          provide: ActivatedRoute,
          useValue: {
              snapshot: {
                  paramMap: {
                      get(): string {
                          return '123';
                      },
                  },
              },
          },
        }, 
        { provide: Router,      useValue: routerSpy }
      ],
      declarations: [ 
        HomeComponent,
        PostStreamComponent,
        PostComponent,
        PostHeaderComponent,
        PostThumbnailComponent,
        PostFooterComponent,
        DetailComponent,
        MatchUrlPipe,
        DateComponent,
        IngredientsComponent,
        KindDescriptionComponent,
        RecipeComponent,
        ViewsComponent,
        AdminControlsComponent
      ]
    })
    .compileComponents();

    injector = getTestBed();
    _apirezzemolo = injector.get(ApirezzemoloService);
    httpMock = injector.get(HttpTestingController);
  }));

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    _search = new SearchService(<any> routerSpy, _ga);
    _apirezzemolo = new ApirezzemoloService(<any> httpClientSpy, _search, <any> routerSpy);
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
});



export function asyncData<T>(data: T) {
  return defer(() => Promise.resolve(data));
}

export function asyncError<T>(errorObject: any) {
    return defer(() => Promise.reject(errorObject));
}
