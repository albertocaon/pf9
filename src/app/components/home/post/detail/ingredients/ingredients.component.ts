import { Component, OnInit, Input } from '@angular/core';
import { Ingredient} from '../../../../../interfaces/detail';
import { SearchService } from '../../../../../services/search.service';

@Component({
  selector: 'app-ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.sass']
})
export class IngredientsComponent implements OnInit {
	@Input() ingredientArray: Ingredient[];

	constructor(public _search: SearchService) { }

	ngOnInit() {
	}
}
