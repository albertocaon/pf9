import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterModule, Router } from '@angular/router';
import { IngredientsComponent } from './ingredients.component';
import { SearchService } from '../../../../../services/search.service';
import { GoogleAnalyticsService } from '../../../../../services/google-analytics.service';

describe('IngredientsComponent', () => {
  let component: IngredientsComponent;
  let fixture: ComponentFixture<IngredientsComponent>;
  let router: { get: jasmine.Spy };
  let _search: SearchService;
  let _ga: GoogleAnalyticsService;
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngredientsComponent ],
      providers: [{ provide: Router, useValue: routerSpy }, GoogleAnalyticsService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    _search = new SearchService(<any> routerSpy, _ga);
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
