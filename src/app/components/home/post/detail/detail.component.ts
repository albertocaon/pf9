import { Component, OnInit, HostListener, ChangeDetectorRef } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { ApirezzemoloService } from '../../../../services/apirezzemolo.service';
import { SearchService } from '../../../../services/search.service';
import { UserService } from '../../../../services/user.service';
import { PostService } from '../../../../services/post.service';
import { Detail, Ingredient } from '../../../../interfaces/detail';
import { User } from '../../../../interfaces/user';
import { FoodType, defaultFoodType } from '../../../../interfaces/food-type';
import { Kinds, Kind, defaultKinds } from '../../../../interfaces/kind';
import { environment } from '../../../../../environments/environment';
import { Meta, Title } from '@angular/platform-browser';
import { Location } from '@angular/common';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.sass']
})
export class DetailComponent implements OnInit {

	idPost: number;
	detail: Detail;
	fileFixedUrl: string = environment.apiUrl + 'file/';
	ingredientArray: Ingredient[];

	@HostListener('window:keydown.escape')
	onPopState() {
		this.location.back();
	}

	constructor(
		private activatedRoute: ActivatedRoute, 
		private _apirezzemolo: ApirezzemoloService,
		private _search: SearchService,
		public router: Router,
		private meta: Meta,
		private titleService: Title,
		private cdr: ChangeDetectorRef,
		public _user: UserService,
		private _post: PostService,
		public location: Location
	) { }

	ngOnInit() {
		if (this.activatedRoute.snapshot.paramMap.get('idPost')) {
			this.idPost = parseInt(this.activatedRoute.snapshot.paramMap.get('idPost'));
			this._apirezzemolo.incrementPostViews(this.idPost).subscribe(() => {
				this.cdr.detectChanges()
			});
			this._search.selectedPostId = this.idPost;

			this._post.getPost(this.idPost).subscribe((detail: Detail) => {
				this.detail = detail;
				this.removeMetaTags();
				if (!this.detail.id) {
					console.info('404 page not found');
					this.router.navigate(['404'])
				} else {
					const titleEncoded = this.buildPrettyTitle(this.detail.title);
					const preattyUrl = this.router.createUrlTree(['detail', this.detail.id, titleEncoded]).toString();
					history.replaceState(null, 'prettyTitle', preattyUrl);
				}
				if (this.detail.ingredients) {
					this.ingredientArray = JSON.parse(this.detail.ingredients);
					let keywords: string = '';
					if (this.detail.vegan == true) {
						keywords += 'Vegan, ';
					}
					if (this.detail.veggie == true) {
						keywords += 'Veggie, ';
					}
					if (this.detail.glutenFree == true) {
						keywords += 'GlutenFree, ';
					}
					if (this.detail.lactoseFree == true) {
						keywords += 'LactoseFree, ';
					}
					this.ingredientArray.forEach((v) => {
						if (v.name) {
							if (v.name.substr(0,1) == '#') {
								keywords += v.name + ', ';
							} else {
								keywords += 'cook with ' + v.name + ', ' 
								+ 'recipe with ' + v.name + ', ' 
								+ 'dish with ' + v.name + ', ' 
								+ 'cocinar con ' + v.name + ', ' 
								+ 'receta con ' + v.name + ', ' 
								+ 'plato con ' + v.name + ', '
								+ 'cuinar amb ' + v.name + ', ' 
								+ 'recepta amb ' + v.name + ', ' 
								+ 'plat amb ' + v.name + ', ' 
								+ 'cucinare con ' + v.name + ', ' 
								+ 'ricetta con ' + v.name + ', ' 
								+ 'piatto con ' + v.name + ', ';
								
							}
						}
					});
					
					if (keywords !== '') {
						keywords = keywords.substring(0, keywords.length-2);
					}
					this.meta.addTags([
					{
						name: "author",
						content: 'Prezzemolo Fresco'
					},
					{
						name: "title",
						content: this.detail.title ? this.detail.title : ''
					},
					{
						name: "description",
						content: this.detail.description ? this.detail.description : ''
					},
					{
						name: "keywords",
						content: keywords ? keywords : ''
					}
					]);

					this.titleService.setTitle('Prezzemolofresco | ' + this.detail.title);
				} else {
					this.ingredientArray = [];
				}
			})
		} else {
			// Redirect the user
			this._search.reset();
			this.router.navigate(['/']);
		}
	}

	removeMetaTags() {
		this.meta.removeTag('name="author"');
	    this.meta.removeTag('name="title"');
	    this.meta.removeTag('name="description"');
	    this.meta.removeTag('name="keywords"');
	}

	buildPrettyTitle(str) {
	  return str.replace(/[^a-zA-Z0-9\'z]/g, '_');
	}
}
