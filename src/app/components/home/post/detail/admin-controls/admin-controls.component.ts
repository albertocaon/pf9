import { Component, OnInit, Input } from '@angular/core';
import { ApirezzemoloService } from '../../../../../services/apirezzemolo.service';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-admin-controls',
  templateUrl: './admin-controls.component.html',
  styleUrls: ['./admin-controls.component.sass']
})
export class AdminControlsComponent implements OnInit {

	@Input() idPost: number;

	constructor(public _apirezzemolo: ApirezzemoloService, private router: Router) { }

	ngOnInit() {
	}

	delete() {
		this._apirezzemolo.deletePost(this.idPost).subscribe(() => {
			this.router.navigate(['/']);
		});
	}

}
