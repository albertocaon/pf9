import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpErrorResponse } from '@angular/common/http';
import { ApirezzemoloService } from '../../../../services/apirezzemolo.service';
import { GoogleAnalyticsService } from '../../../../services/google-analytics.service';
import { SearchService } from '../../../../services/search.service';
import { DetailComponent } from './detail.component';
import { AdminControlsComponent } from './admin-controls/admin-controls.component';
import { DateComponent } from './date/date.component';
import { IngredientsComponent } from './ingredients/ingredients.component';
import { KindDescriptionComponent } from './kind-description/kind-description.component';
import { RecipeComponent } from './recipe/recipe.component';
import { ViewsComponent } from './views/views.component';
import { RouterModule, ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MatchUrlPipe } from '../../../../pipes/match-url.pipe';
import { Router } from '@angular/router';
import { MomentModule } from 'ngx-moment';

describe('DetailComponent', () => {
  let component: DetailComponent;
  let fixture: ComponentFixture<DetailComponent>;
  let injector: TestBed;
  let _apirezzemolo: ApirezzemoloService;
  let _search: SearchService;
  let _ga: GoogleAnalyticsService;
  let httpMock: HttpTestingController;
  let httpClientSpy: { get: jasmine.Spy };
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({ 
      imports: [ 
        HttpClientTestingModule,
        RouterTestingModule,
        MomentModule
      ],
      declarations: [ 
        DetailComponent,
        MatchUrlPipe,
        DateComponent,
        IngredientsComponent,
        KindDescriptionComponent,
        RecipeComponent,
        ViewsComponent,
        AdminControlsComponent      
      ],
      providers: [ 
        ApirezzemoloService,
        GoogleAnalyticsService,
        { provide: Router, useValue: routerSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get(): number {
                  return 6;
                }
              }
            }
          }
        }
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {

    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    _search = new SearchService(<any> routerSpy, _ga);
    _apirezzemolo = new ApirezzemoloService(<any> httpClientSpy, _search, <any> routerSpy);
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
