import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KindDescriptionComponent } from './kind-description.component';

describe('KindDescriptionComponent', () => {
  let component: KindDescriptionComponent;
  let fixture: ComponentFixture<KindDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KindDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KindDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
