import { Component, OnInit, Input } from '@angular/core';
import { Detail } from '../../../../../interfaces/detail';
import { Kinds, Kind, defaultKinds } from '../../../../../interfaces/kind';

@Component({
  selector: 'app-kind-description',
  templateUrl: './kind-description.component.html',
  styleUrls: ['./kind-description.component.sass']
})
export class KindDescriptionComponent implements OnInit {

	@Input() detail: Detail;
	currentKinds: Kind[] = [];

	constructor() { }

	ngOnInit() {
		if (this.detail) {
			this.evaluateCurrentKinds();
		}
	}

	evaluateCurrentKinds() {
		this.currentKinds = [];
		for (let [key, value] of Object.entries(defaultKinds)) {
			if (this.detail[key] == 1) {
				const kind: Kind = value;
				this.currentKinds.push(kind);
			}	
		}
	}
}
