import { Component, OnInit, Input, OnChanges, SimpleChange, SimpleChanges, ChangeDetectorRef } from '@angular/core';
import { Post } from '../../../../interfaces/post';
import { UserService } from '../../../../services/user.service';

@Component({
  selector: 'app-post-header',
  templateUrl: './post-header.component.html',
  styleUrls: ['./post-header.component.sass']
})
export class PostHeaderComponent implements OnInit, OnChanges {

	@Input() post: Post;

	constructor(private cdr: ChangeDetectorRef) { }

	ngOnInit() {
	}

	ngOnChanges(changes: SimpleChanges) {
	    if (changes.post) {
	      this.post = changes.post.currentValue;
	      this.cdr.detectChanges();
	    }
	}

}
