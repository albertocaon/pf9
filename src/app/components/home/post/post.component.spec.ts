import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PostComponent } from './post.component';
import { PostHeaderComponent } from './post-header/post-header.component';
import { PostThumbnailComponent } from './post-thumbnail/post-thumbnail.component';
import { PostFooterComponent } from './post-footer/post-footer.component';
import { DetailComponent } from './detail/detail.component';
import { ApirezzemoloService } from '../../../services/apirezzemolo.service';
import { SearchService } from '../../../services/search.service';
import { GoogleAnalyticsService } from '../../../services/google-analytics.service';
import { dummyPosts } from '../../../mocks/posts.mock';
import { RouterModule } from '@angular/router';
import { MatchUrlPipe } from '../../../pipes/match-url.pipe';
import { MomentModule } from 'ngx-moment';
import { DateComponent } from './detail/date/date.component';
import { IngredientsComponent } from './detail/ingredients/ingredients.component';
import { KindDescriptionComponent } from './detail/kind-description/kind-description.component';
import { RecipeComponent } from './detail/recipe/recipe.component';
import { ViewsComponent } from './detail/views/views.component';
import { Router } from '@angular/router';
import { AdminControlsComponent } from './detail/admin-controls/admin-controls.component';
import { UserService } from '../../../services/user.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('PostComponent', () => {
  let component: PostComponent;
  let fixture: ComponentFixture<PostComponent>;
  let httpMock: HttpTestingController;
  let httpClientSpy: { get: jasmine.Spy };
  let _user: UserService;
  let _apirezzemolo: ApirezzemoloService;
  let _search: SearchService;
  let _ga: GoogleAnalyticsService;
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PostComponent,
        PostHeaderComponent,
        PostThumbnailComponent,
        PostFooterComponent,
        DetailComponent,
        MatchUrlPipe,
        DateComponent,
        IngredientsComponent,
        KindDescriptionComponent,
        RecipeComponent,
        ViewsComponent,
        AdminControlsComponent
      ],
      imports: [
        HttpClientTestingModule,
        RouterModule,
        MomentModule
      ],
      providers: [
        UserService,
        ApirezzemoloService,
        { provide: Router, useValue: routerSpy },
        GoogleAnalyticsService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(() => {
    _user = TestBed.get(UserService);
    _search = new SearchService(<any> routerSpy, _ga);
  })

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
});
