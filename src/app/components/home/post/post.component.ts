import { Component, 
	OnInit, 
	Input, 
	OnChanges, 
	SimpleChange, 
	SimpleChanges, 
	ChangeDetectorRef, 
	Renderer2, 
	ViewChild,
	ElementRef,
	AfterViewInit
} from '@angular/core';
import { Post } from '../../../interfaces/post';
import { SearchService } from '../../../services/search.service';
import { CustomService } from '../../../services/custom.service';
import { UserService } from '../../../services/user.service';
import { GoogleAnalyticsService } from '../../../services/google-analytics.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.sass'],
})
export class PostComponent implements OnInit, OnChanges, AfterViewInit {

  constructor(
  	private cdr: ChangeDetectorRef, 
  	private _search: SearchService, 
	private _custom: CustomService, 
	public _user: UserService,   	
	private renderer: Renderer2,
	private _ga: GoogleAnalyticsService
	) { }

	@Input() post: Post;
	@ViewChild("header") header: ElementRef;
	bgColor:string = 'blackBg';

	ngOnInit() {
		this.bgColor = this._custom.bgColor[this._custom.bgColorSelected%this._custom.numberOfBgColors] + 'Bg';
	}

	ngAfterViewInit() {
		this._custom.bgColorEvent.subscribe((color) => {
			this.bgColor = this._custom.bgColor[this._custom.bgColorSelected%this._custom.numberOfBgColors] + 'Bg';
		});
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes.post) {
		  this.post = changes.post.currentValue;
		  this.cdr.detectChanges();
		}
	}

	seePostDetailsEventTracker(origin: string) {
		this._ga.eventEmitter(localStorage.getItem('isLoggedIn') ? 'admin-see_post_details' : 'see_post_details', 'browse', 'click', 'postListClickOn_' + origin + this.post.title, 10);
	}
}
