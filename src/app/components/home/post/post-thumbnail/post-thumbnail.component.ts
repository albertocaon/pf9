import { Component, OnInit, Input, OnChanges, SimpleChange, SimpleChanges, ChangeDetectorRef } from '@angular/core';
import { Post } from '../../../../interfaces/post';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-post-thumbnail',
  templateUrl: './post-thumbnail.component.html',
  styleUrls: ['./post-thumbnail.component.sass']
})
export class PostThumbnailComponent implements OnInit, OnChanges {

	@Input() post: Post;
	thumbnailFixedUrl: string = environment.apiUrl + 'thumbnail/';

	constructor(private cdr: ChangeDetectorRef) { }

	ngOnInit() {
	}

	ngOnChanges(changes: SimpleChanges) {
	    if (changes.post) {
	      this.post = changes.post.currentValue;
	      this.cdr.detectChanges();
	    }
	}
}
