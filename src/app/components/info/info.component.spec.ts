import { TestBed, ComponentFixture, fakeAsync, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { InfoComponent } from './info.component';

describe('InfoComponent', () => {
  let component: InfoComponent;
  let fixture: ComponentFixture<InfoComponent>;
  const externalLinksCounter: number = 3;
  const internalLinksCounter: number = 14;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have all external links', () => {
    const externalLinks = fixture.debugElement.queryAll(By.css('.externalLink'));
    expect(externalLinks.length).toEqual(externalLinksCounter);
  });

  it('should have all internal links', () => {
    const internalLinks = fixture.debugElement.queryAll(By.css('.internalLink'));
    expect(internalLinks.length).toEqual(internalLinksCounter);
  });

  it('external links should call tracking function', fakeAsync( () => {
    fixture.detectChanges();
    spyOn(component, 'trackExternalLink');
    const links = fixture.debugElement.queryAll(By.css('.externalLink'));
    links.forEach((link) => {
      link.triggerEventHandler('click', null);
      tick();
      fixture.detectChanges();
      expect(component.trackExternalLink).toHaveBeenCalled();
    })
  }));

  it('internal links should call tracking function', fakeAsync( () => {
    fixture.detectChanges();
    spyOn(component, 'trackHashtagClick');
    const links = fixture.debugElement.queryAll(By.css('.internalLink'));
    links.forEach((link) => {
      link.triggerEventHandler('click', null);
      tick();
      fixture.detectChanges();
      expect(component.trackHashtagClick).toHaveBeenCalled();
    })
  }));
});
