import { Component, OnInit } from '@angular/core';
import { GoogleAnalyticsService } from '../../services/google-analytics.service';

@Component({
  selector: 'pf9-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.sass']
})
export class InfoComponent implements OnInit {

	constructor(private _ga: GoogleAnalyticsService) { }

	ngOnInit(): void {
		this._ga.eventEmitter(localStorage.getItem('isLoggedIn') ? 'admin-about-me' : 'about-me', 'browse', 'reading', 'displaying personal info: about me');
	}

	trackHashtagClick(name: string) {
		this._ga.eventEmitter(localStorage.getItem('isLoggedIn') ? 'admin-hashtag' : 'hashtag', 'browse', 'hashtagFromPersonalInfoClick', 'goingToHashtag: ' + name, 20);
	}

	trackExternalLink(link: string) {
		this._ga.eventEmitter(localStorage.getItem('isLoggedIn') ? 'admin-hashtag' : 'hashtag', 'browse', 'externalLink', 'goingToExternal: ' + link, 20);
	}
}
