import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApirezzemoloService } from '../../services/apirezzemolo.service';
import { SearchService } from '../../services/search.service';
import { GoogleAnalyticsService } from '../../services/google-analytics.service';
import { Hashtag } from '../../interfaces/hashtag';
import { Meta, Title } from '@angular/platform-browser';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'hashtags',
  templateUrl: './hashtags.component.html',
  styleUrls: ['./hashtags.component.sass']
})
export class HashtagsComponent implements OnInit, OnDestroy {

  constructor(public _apirezzemolo: ApirezzemoloService, public _search: SearchService, private meta: Meta, private titleService: Title, private _ga: GoogleAnalyticsService) { }

  hashtags: Hashtag[];
  matchingHashtags: Hashtag[];
  input = new FormControl();


  ngOnInit() {
    this.removeMetaTags();
  	this.getHashtags();
    this.initializeInput();
  }

  getHashtags() {
  	this._apirezzemolo.getHashtags().subscribe( (hashtags) => {
      this.hashtags = hashtags;
      this.matchingHashtags = hashtags;
      let hashtagsConcat: string = '';
      this.hashtags.forEach((h, k) => {
        hashtagsConcat += '#' + h.name;
        if (k < this.hashtags.length - 1) {
          hashtagsConcat += ', ';
        }
      });
      this.meta.addTags([
        {
          name: "author",
          content: "Prezzemolofresco's hashtags"  
        },
        {
          name: "description",
          content: "Hashtags of Prezzemolofresco's recipes, ingredients and custom filters"
        },
        {
          name: "keywords",
          content: hashtagsConcat
        }
      ]);
    });
  }

  ngOnDestroy() {
    this.removeMetaTags();
  }

  searchHashtags() {
    this.initializeInput();
    this._apirezzemolo.getMatchingHashtags(this.input.value).subscribe( (hashtags) => {
      this.matchingHashtags = hashtags;
    });
  }

  initializeInput() {
    if (this.input && this.input.value && this.input.value.substring(0, 1) !== '#') {
      this.input.setValue('#' + this.input.value);
    }
  }

  removeMetaTags() {
    this.meta.removeTag('name="author"');
      this.meta.removeTag('name="title"');
      this.meta.removeTag('name="description"');
      this.meta.removeTag('name="keywords"');
  }

  trackHashtagClick(name: string) {
    this._ga.eventEmitter(localStorage.getItem('isLoggedIn') ? 'admin-hashtag' : 'hashtag', 'browse', 'hashtagListClick', 'goingToHashtag: ' + name, 20);
  }
}
