import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { SearchService } from '../../../services/search.service';
import { KindSelectorComponent } from './kind-selector.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { Kinds, Kind, defaultKinds } from '../../../interfaces/kind';
import { FoodType, defaultFoodType } from '../../../interfaces/food-type';
import { Router } from '@angular/router';

describe('KindSelectorComponent', () => {
  let injector: TestBed;
  let component: KindSelectorComponent;
  let fixture: ComponentFixture<KindSelectorComponent>;
  let searchService: SearchService;
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        KindSelectorComponent
      ],
      imports: [
        MatInputModule,
        MatSelectModule,
        MatFormFieldModule, 
        FormsModule, 
        ReactiveFormsModule,
        BrowserAnimationsModule
      ],
      providers: [
        SearchService,
        { provide: Router, useValue: routerSpy }
      ]
    })
    .compileComponents();
    injector = getTestBed();
    searchService = injector.get(SearchService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KindSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have kinds object', () => {
    expect(component.kinds).toBeDefined();
  });

  it('check the length of kinds drop down', async () => {
    component.ngOnInit();
    const trigger = fixture.debugElement.query(By.css('.mat-select-trigger')).nativeElement;
    trigger.click();
    fixture.detectChanges();
    await fixture.whenStable().then(() => {
        const inquiryOptions = fixture.debugElement.queryAll(By.css('.mat-option-text'));
        expect(inquiryOptions.length).toEqual(component.kindNames.length);
    });
  });

  it('vegan option should have status according to variable', async () => {
    component.ngOnInit();
    component.kinds.vegan.checked = false;
    const trigger = fixture.debugElement.query(By.css('.mat-select-trigger')).nativeElement;
    trigger.click();
    fixture.detectChanges();
    await fixture.whenStable().then(async () => {
        const veganOption: HTMLElement = fixture.debugElement.query(By.css('#vegan')).nativeElement;
        veganOption.click();
        fixture.detectChanges();

        await fixture.whenStable().then(() => {
          expect(component.kinds.vegan.checked).toBeTruthy();
        })
    });
  });

  it('if vegan option is true, veggie and lactose free should be true too', async () => {
    component.ngOnInit();
    component.kinds.vegan.checked = false;
    const trigger = fixture.debugElement.query(By.css('.mat-select-trigger')).nativeElement;
    trigger.click();
    fixture.detectChanges();
    await fixture.whenStable().then(async () => {
        const veganOption: HTMLElement = fixture.debugElement.query(By.css('#vegan')).nativeElement;
        veganOption.click();
        fixture.detectChanges();

        await fixture.whenStable().then(() => {
          expect(component.kinds.vegan.checked).toBeTruthy();
          expect(component.kinds.veggie.checked).toBeTruthy();
          expect(component.kinds.lactoseFree.checked).toBeTruthy();
        })
    });
  });

  it('search post params veggie should be selected when veggie is selected', async () => {
    component.ngOnInit();
    component.kinds.veggie.checked = false;
    const trigger = fixture.debugElement.query(By.css('.mat-select-trigger')).nativeElement;
    trigger.click();
    fixture.detectChanges();
    await fixture.whenStable().then(async () => {
      const veggieOption: HTMLElement = fixture.debugElement.query(By.css('#veggie')).nativeElement;
      veggieOption.click();
      fixture.detectChanges();
      await fixture.whenStable().then(() => {
        expect(searchService.searchPostsParams.foodType.veggie).toBeTruthy();
      });
    });
  });

  it('search post params vegan, veggie and lactoseFree should be selected when vegan is clicked', async () => {
    component.ngOnInit();
    component.kinds.vegan.checked = false;
    const trigger = fixture.debugElement.query(By.css('.mat-select-trigger')).nativeElement;
    trigger.click();
    fixture.detectChanges();
    await fixture.whenStable().then(async () => {
      const veganOption: HTMLElement = fixture.debugElement.query(By.css('#vegan')).nativeElement;
      veganOption.click();
      fixture.detectChanges();
      await fixture.whenStable().then(() => {
        expect(searchService.searchPostsParams.foodType.vegan).toBeTruthy();
        expect(searchService.searchPostsParams.foodType.veggie).toBeTruthy();
        expect(searchService.searchPostsParams.foodType.lactoseFree).toBeTruthy();
      });
    });
  });

  it('When selected and clicked again the same option should be deselected', async () => {
    component.ngOnInit();
    component.kinds.veggie.checked = false;
    const trigger = fixture.debugElement.query(By.css('.mat-select-trigger')).nativeElement;
    trigger.click();
    fixture.detectChanges();
    await fixture.whenStable().then(async () => {
      const veggieOption: HTMLElement = fixture.debugElement.query(By.css('#veggie')).nativeElement;
      veggieOption.click();
      fixture.detectChanges();
      veggieOption.click();
      fixture.detectChanges();
      await fixture.whenStable().then(() => {
        expect(searchService.searchPostsParams.foodType.veggie).toBeFalsy();
      });
    });
  });

  it('When selected vegan and deselected veggie, vegan should be deselected too but lactoseFree still selected', async () => {
    component.ngOnInit();
    component.kinds.vegan.checked = false;
    component.kinds.veggie.checked = false;
    component.kinds.lactoseFree.checked = false;

    const trigger = fixture.debugElement.query(By.css('.mat-select-trigger')).nativeElement;
    trigger.click();
    fixture.detectChanges();
    await fixture.whenStable().then(async () => {
      const veganOption: HTMLElement = fixture.debugElement.query(By.css('#vegan')).nativeElement;
      const veggieOption: HTMLElement = fixture.debugElement.query(By.css('#veggie')).nativeElement;
      const lactoseFreeOption: HTMLElement = fixture.debugElement.query(By.css('#lactoseFree')).nativeElement;

      veganOption.click();
      fixture.detectChanges();
      veggieOption.click();
      fixture.detectChanges();
      await fixture.whenStable().then(() => {
        expect(searchService.searchPostsParams.foodType.vegan).toBeFalsy();
        expect(searchService.searchPostsParams.foodType.veggie).toBeFalsy();
        expect(searchService.searchPostsParams.foodType.lactoseFree).toBeTruthy();
      });
    });
  });

  it('convert searchParams into kinds', () => {
    component.ngOnInit();
    searchService.searchPostsParams.foodType = defaultFoodType;
    searchService.searchPostsParams.foodType.veggie = true;
    component.convertSearchParamsIntoKinds();
    const veggieKind = component.kinds['veggie'];
    expect(veggieKind.checked).toBeTruthy();
  });
});
