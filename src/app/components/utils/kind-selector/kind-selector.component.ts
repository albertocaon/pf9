import { Component, OnInit } from '@angular/core';
import { Kinds, Kind, defaultKinds } from '../../../interfaces/kind';
import { FoodType, defaultFoodType } from '../../../interfaces/food-type';
import { SearchService } from '../../../services/search.service';
import { GoogleAnalyticsService } from '../../../services/google-analytics.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-kind-selector',
  templateUrl: './kind-selector.component.html',
  styleUrls: ['./kind-selector.component.sass']
})
export class KindSelectorComponent implements OnInit {

	kindNames: string[] = ['vegan', 'veggie', 'glutenFree', 'lactoseFree'];
	kinds: Kinds;

	checkedOptions = new FormControl();

	constructor(private _search: SearchService, private _ga: GoogleAnalyticsService) { }

	ngOnInit() {
		this.kinds = JSON.parse(JSON.stringify(defaultKinds));
		this.convertSearchParamsIntoKinds();

		this._search.setDefaultKinds.subscribe(() => {
			this.kinds = JSON.parse(JSON.stringify(defaultKinds));
			this.checkedOptions = new FormControl([]);
		});
	}

	coherence(selected) {
		let checked: string[] = this.checkedOptions.value;
		this.kinds[selected].checked = !this.kinds[selected].checked;
		const isChecked: boolean = this.kinds[selected].checked;
		this._search.searchPostsParams.foodType = defaultFoodType;
		this._search.searchPostsParams.foodType[selected] = isChecked;
	 	checked = this.triggerConsequentTruths(selected, checked, isChecked);
	 	checked = this.triggerConsequentFalses(selected, checked, isChecked);
	 	this.checkedOptions = new FormControl(checked);
	 	this._search.searchPostsParams.pageNumber = 0;
	 	this._search.searchPostsParamsChanged.emit(true);
	 	this._ga.eventEmitter(localStorage.getItem('isLoggedIn') ? 'admin-kindSelector' : 'kindSelector', 'search', 'kindSelect', 'searchingForKinds: ' + JSON.stringify(checked), 20);
	}

	triggerConsequentTruths(selected, checked, isChecked) {
		for (let trueTriggered of this.kinds[selected].conditionalTriggers[isChecked ? 'isChecked' : 'isNotChecked'].truths) {
	 		this.kinds[trueTriggered].checked = true;
	 		checked[trueTriggered] ? '' : checked.push(trueTriggered);
	 		checked = [...new Set(checked)];
	 		this._search.searchPostsParams.foodType[trueTriggered] = true;
	 	}

	 	return checked;
	}

	triggerConsequentFalses(selected, checked, isChecked) {
		for (let falseTriggered of this.kinds[selected].conditionalTriggers[isChecked ? 'isChecked' : 'isNotChecked'].falses) {
	 		this.kinds[falseTriggered].checked = false;
	 		checked = checked.filter((kind: string) => kind !== falseTriggered);
	 		checked = [...new Set(checked)];
	 		this._search.searchPostsParams.foodType[falseTriggered] = false;
	 	}

	 	return checked;
	}

	convertSearchParamsIntoKinds() {
		let checked: string[] = [];
		if (this._search.searchPostsParams && this._search.searchPostsParams.foodType) {
			for (let [key, value] of Object.entries(this._search.searchPostsParams.foodType)) {
				this.kinds[key].checked = value;
				if (value) {
					checked.push(key);
				}
			}
			this.checkedOptions = new FormControl(checked);
		}
	}
}