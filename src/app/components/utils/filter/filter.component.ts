import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { SearchService } from '../../../services/search.service';
import { GoogleAnalyticsService } from '../../../services/google-analytics.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.sass']
})
export class FilterComponent implements OnInit {

  constructor(private _search: SearchService, private _ga: GoogleAnalyticsService) { }
  input = new FormControl();

  ngOnInit() {
    this.input.setValue(this._search.searchPostsParams.filters || null);
    this._search.searchPostsParamsChanged.emit(true);
    
    this._search.setEmptyFilter.subscribe(() => {
      this.input.setValue(null);
      this._search.searchPostsParamsChanged.emit(true);
    });
  }

  onTypingSearch() {
  	this._search.searchPostsParams.filters = this.input.value;
    this._search.searchPostsParams.pageNumber = 0;
    this._search.searchPostsParamsChanged.emit(true);
    this._ga.eventEmitter(localStorage.getItem('isLoggedIn') ? 'admin-filtering' : 'filtering', 'search', 'typing', 'searchingForText: ' + this.input.value, 20);
  }
}
