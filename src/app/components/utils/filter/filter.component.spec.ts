import { async, ComponentFixture, TestBed, getTestBed, inject } from '@angular/core/testing';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FilterComponent } from './filter.component';
import { By } from '@angular/platform-browser';
import { SearchService } from '../../../services/search.service';
import { GoogleAnalyticsService } from '../../../services/google-analytics.service';
import { PostService } from '../../../services/post.service';
import { Post } from '../../../interfaces/post';
import { RouterModule, Router } from '@angular/router';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('FilterComponent', () => {
  let injector: TestBed;
  let component: FilterComponent;
  let fixture: ComponentFixture<FilterComponent>;
  let searchService: SearchService;
  let gaService: GoogleAnalyticsService;
  let postService: PostService;
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterComponent ],
      imports: [
        MatInputModule,
        MatFormFieldModule, 
        FormsModule, 
        ReactiveFormsModule,
        BrowserAnimationsModule,
        HttpClientTestingModule
      ],
      providers: [
        SearchService, 
        GoogleAnalyticsService,
        PostService,
        { provide: Router,      useValue: routerSpy }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterComponent);
    component = fixture.componentInstance;
    injector = getTestBed();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set searchText when typed', async(inject([SearchService, GoogleAnalyticsService], (searchService: SearchService, gaService: GoogleAnalyticsService) => {
    fixture.detectChanges();
    const input = fixture.debugElement.query(By.css('.searchInput'));
    input.nativeElement.value = 'a';
    input.nativeElement.dispatchEvent(new Event('input'));
    component.onTypingSearch();
    expect(searchService.searchPostsParams.filters).toEqual('a');
  })));

  it('filter input according to searchParams', async(inject([SearchService, GoogleAnalyticsService], (searchService: SearchService, gaService: GoogleAnalyticsService) => {
    fixture.detectChanges();
    searchService.searchPostsParams.filters = 'wasabi';
    component.ngOnInit();

    expect(component.input.value).toEqual('wasabi');
  })));

  // it('results are compliant with search term', async(inject([
  //   SearchService, 
  //   PostService
  // ], (
  //   searchService: SearchService, 
  //   postService: PostService
  // ) => {
  //   fixture.detectChanges();
  //   const searchWord = 'pan';
  //   searchService.searchPostsParams.filters = searchWord;
  //   component.ngOnInit();
  //   fixture.detectChanges();

  //   let posts = [];
  //   postService.getPosts(searchService.searchPostsParams).subscribe((result: Post[]) => {
  //       posts = result;
  //   });
  //   jasmine.clock().install();
  //   jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
  //   jasmine.clock().tick(3000);

  //   console.log(posts);
  //   let compliant = true;
  //   posts.forEach((post) => {
  //     console.log('title', post.title.toLowerCase());
  //     console.log('desc', post.description.toLowerCase());
  //     if (post.title.toLowerCase().indexOf(searchWord) < 0 && post.description.toLowerCase().indexOf(searchWord) < 0) {
  //       compliant = false;
  //     }
  //   });
  //   expect(compliant).toBeTruthy();
  // })));
});