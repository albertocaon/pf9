import { Component, DebugElement } from "@angular/core";
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MenuComponent } from './menu.component';
import { Router, ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { KindSelectorComponent } from '../utils/kind-selector/kind-selector.component';
import { FilterComponent } from '../utils/filter/filter.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ApirezzemoloService } from '../../services/apirezzemolo.service';
import { UserService } from '../../services/user.service';
import { By } from '@angular/platform-browser';

describe('MenuComponent', () => {
  let component: MenuComponent;
  let fixture: ComponentFixture<MenuComponent>;
  let httpMock: HttpTestingController;
  let httpClientSpy: { get: jasmine.Spy };
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
  let _apirezzemolo: ApirezzemoloService;
  let _user: UserService;
  let el: DebugElement;
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule, 
        MatTabsModule,
        MatMenuModule,
        MatInputModule,
        MatSelectModule,
        MatFormFieldModule, 
        MatButtonModule,
        FormsModule, 
        ReactiveFormsModule,
        BrowserAnimationsModule
      ],
      declarations: [ 
        MenuComponent,
        KindSelectorComponent,
        FilterComponent
      ],
      providers: [ 
        UserService,
        ApirezzemoloService,
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: {
                get(): number {
                  return 6;
                }
              }
            }
          }
        }
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuComponent);
    component = fixture.componentInstance;
    _user = TestBed.get(UserService);
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have navLinks defined', () => {
    expect(component.navLinks).toBeDefined();
  });

  it('navLinks should have proper values', () => {
    const mockedNavLinks = [
      {
        path: 'home',
        label: 'Recetas',
        icon: 'restaurant_menu'
      },
      {
        path: 'hashtags',
        label: 'Hashtags',
        icon: 'forward'
      },
      {
        path: 'info',
        label: 'Quién soy',
        icon: 'sentiment_very_satisfied'
      },  
      {
        path: 'login',
        label: 'Admin',
        icon: 'lock'
      }
    ];
    expect(component.navLinks).toEqual(mockedNavLinks);
  });

  it('should have adminLinks defined', () => {
    expect(component.adminLinks).toBeDefined();
  });

  it('adminLinks should have proper values', () => {
    const mockedAdminLinks = [
      {
        path: 'dish',
        label: 'New Dish',
        icon: 'add'
      },
      {
        path: 'top',
        label: 'Top 20',
        icon: 'star'
      },
      // {
      //   path: 'settings',
      //   label: 'Settings',
      //   icon: 'settings'
      // },
      {
        path: '/',
        label: 'Log out',
        icon: 'clear'
      }
    ];
    expect(component.adminLinks).toEqual(mockedAdminLinks);
  });

  it('schould not display Admin Menu if not logged in', () => {
    spyOn(_user, 'isLoggedIn').and.returnValue(false);
    fixture.detectChanges();
    el = fixture.debugElement.query(By.css('#adminMenuTrigger'));
    expect(el).toBeNull();
  });

  it('schould display Admin Menu if logged in', () => {
    spyOn(_user, 'isLoggedIn').and.returnValue(true);
    fixture.detectChanges();
    el = fixture.debugElement.query(By.css('#adminMenuTrigger'));
    expect(el.nativeElement).toBeTruthy();
  })
});
