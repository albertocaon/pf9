import { Component, OnInit, ElementRef, Renderer2 } from '@angular/core';
import { navLinks, adminLinks, NavLink } from '../../interfaces/menu';
import { SearchService } from '../../services/search.service';
import { UserService } from '../../services/user.service';
import { CustomService } from '../../services/custom.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.sass']
})
export class MenuComponent implements OnInit {

	navLinks: NavLink[];
	adminLinks: NavLink[];
	bgColor: string[] = ['black', 'white'];
	numberOfBgColors: number = 2;
	bgColorSelected: number = 0; 
  	
	constructor(
		public _search: SearchService,
		public _user: UserService,
		public _custom: CustomService,
    	public router: Router,
    	public location: Location,
    	private el: ElementRef, 
    	private renderer: Renderer2
	) { }

	ngOnInit() {
		this.navLinks = navLinks;
		this.adminLinks = adminLinks;
		this._custom.bgColorEvent.subscribe((color: string) => {
			this.renderer.setStyle(this.el.nativeElement.ownerDocument.body,'backgroundColor', color);
		});
	}

	goBack() {
    if (history.length > 2) {
      history.back();
    } else {
      this.router.navigate(['/']);
    }
  }
}
