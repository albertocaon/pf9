import { Component, OnInit, AfterViewInit } from '@angular/core';
import { RouterOutlet, Router, NavigationEnd } from '@angular/router';
import { slideInAnimation } from './route-animations';
import { CustomService } from './services/custom.service';
import { GoogleAnalyticsService } from './services/google-analytics.service';

declare let gtag: Function;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
  animations: [
  	slideInAnimation
  	// slider,
  	// fader
  ]
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'pf9';
  bgColor:string = 'blackBg';

  constructor(private _custom: CustomService, public router: Router, private _ga: GoogleAnalyticsService) {}

  ngOnInit() {
  		this.router.events.subscribe(event => {
  			if (event instanceof NavigationEnd) {
  				gtag('config', 'G-DW9HSYBPZK',
  				{
  					'page_path': event.urlAfterRedirects
  				});
  			}
  		});
  		
  		this.bgColor = this._custom.bgColor[this._custom.bgColorSelected%this._custom.numberOfBgColors] + 'Bg';
		console.info(	
		  "%c                                                     \n" +  
		  "                                                     \n" + 
		  "   /////////////////             %c  ,****             \n" +
		  "   %c///////////////////       %c  ,**.***** ,**.        \n" +
		  "   %c/////*       ///////      %c  **************        \n" +
		  "   %c/////*         /////*       %c  .********           \n" +
		  "   %c/////*         /////*     %c  **************        \n" +
		  "   %c/////*       ,//////       %c  ,**********.         \n" +
		  "   %c///////////////////    %c  ., ***,,*** *,***.,.     \n" +
		  "   %c//////////////////    %c  *********  ,  ********    \n" +
		  "   %c/////*                 %c  ,*******  ,  *******,    \n" +
		  "   %c/////*                %c  *********, , *********    \n" +
		  "   %c/////*                   %c  **      ,     ***      \n" +
		  "   %c/////*                                            \n" +
			"   /////*                                            \n" +
			"   /////*                     //////////             \n" +
			"                              ///                    \n" +
			"                              ///                    \n" +
			"   //////////////////////     ////////*              \n" +
			"                              ///                    \n" +
			"                              ///                    \n" +
			"                              ///                    \n" +
			"                              ///                    \n" +
			"                                                     \n" +
			"                                                     \n", 
			"display: inline-block; max-width: 100%; color: purple; background: #000;", 
			"display: inline-block; max-width: 100%; color: green; background: #000;",
			"display: inline-block; max-width: 100%; color: purple; background: #000;", 
			"display: inline-block; max-width: 100%; color: green; background: #000;",
			"display: inline-block; max-width: 100%; color: purple; background: #000;", 
			"display: inline-block; max-width: 100%; color: green; background: #000;",
			"display: inline-block; max-width: 100%; color: purple; background: #000;", 
			"display: inline-block; max-width: 100%; color: green; background: #000;",
			"display: inline-block; max-width: 100%; color: purple; background: #000;", 
			"display: inline-block; max-width: 100%; color: green; background: #000;",
			"display: inline-block; max-width: 100%; color: purple; background: #000;", 
			"display: inline-block; max-width: 100%; color: green; background: #000;",
			"display: inline-block; max-width: 100%; color: purple; background: #000;", 
			"display: inline-block; max-width: 100%; color: green; background: #000;",
			"display: inline-block; max-width: 100%; color: purple; background: #000;", 
			"display: inline-block; max-width: 100%; color: green; background: #000;",
			"display: inline-block; max-width: 100%; color: purple; background: #000;", 
			"display: inline-block; max-width: 100%; color: green; background: #000;",
			"display: inline-block; max-width: 100%; color: purple; background: #000;", 
			"display: inline-block; max-width: 100%; color: green; background: #000;",
			"display: inline-block; max-width: 100%; color: purple; background: #000;", 
			"display: inline-block; max-width: 100%; color: green; background: #000;", 
			"display: inline-block; max-width: 100%; color: purple; background: #000;");
	}

	ngAfterViewInit() {
		this._custom.bgColorEvent.subscribe((color) => {
			this.bgColor = this._custom.bgColor[this._custom.bgColorSelected%this._custom.numberOfBgColors] + 'Bg';
		});
	}

	prepareRoute(outlet: RouterOutlet) {
		return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
	}

	trackLcClick() {
		this._ga.eventEmitter(localStorage.getItem('isLoggedIn') ? 'admin-lc' : 'lc', 'browse', 'lcFooterLink', 'goingToLc', 20);
	}
}
