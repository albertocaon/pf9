import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Post } from '../interfaces/post';
import { Detail } from '../interfaces/detail';
import { User } from '../interfaces/user';
import { FoodType } from '../interfaces/food-type';
import { UserAbstract } from '../interfaces/user-abstract';
import { Entry } from '../interfaces/entry';
import { environment } from '../../environments/environment';
import { SearchPostsParams } from '../interfaces/search-posts-params';
import { SearchService } from './search.service';
import { Router, NavigationExtras } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ApirezzemoloService {

  post: Post;
  	entry: Entry;
  	user: User = null;
  	apiCnct: boolean = false;
  	isLoggedIn: boolean;
  	totalPosts: number;
  	comingFromTopViewed: boolean = false;
  	topViewedQuantity: number = 0;

  	// store the URL so we can redirect after logging in
    redirectUrl: string;

	constructor(
		private http: HttpClient, 
		private _search: SearchService, 
		private router: Router 
	) {}

	private handleError(error: HttpErrorResponse) {
	  if (error.error instanceof ErrorEvent) {
	    // A client-side or network error occurred. Handle it accordingly.
	    console.error('An error occurred:', error.error.message);
	  } else {
	    // The backend returned an unsuccessful response code.
	    // The response body may contain clues as to what went wrong,
	    console.error(
	      `Backend returned code ${error.status}, ` +
	      `body was: ${error.error}`);
	  }
	  // return an observable with a user-facing error message
	  return throwError(
	    'Something bad happened; please try again later.');
	};

	ping() {
 		return this.http.get<any>(environment.apiUrl + 'ping')
	 		.pipe(
	      		retry(3), // retry a failed request up to 3 times
      			catchError(this.handleError) // then handle the error
	    	);
 	}

 	getPosts(searchParams: SearchPostsParams): Observable<Object> {
		const params = this.buildParamsUrl(searchParams);
 		return this.http.get(environment.apiUrl + 'getposts' + params);
	}

	getPostsCount(searchParams: SearchPostsParams): Observable<Object> {
		const params = this.buildParamsUrl(searchParams);
 		return this.http.get(environment.apiUrl + 'getpostscount' + params);
	}

	getPost(postId: number): Observable<any> {
		return this.http.get(environment.apiUrl + 'post/' + postId);
	}

	getAuthor(userId: number): Observable<any> {
		return this.http.get(environment.apiUrl + 'author/' + userId);
	}

	incrementPostViews(idPost: number): Observable<any> {
		return this.http.post(environment.apiUrl + 'incrementpostviews', {postId: idPost});
	}

	getHashtags(): Observable<any> {
		return this.http.get(environment.apiUrl + 'hashtags');
	}

	getTop(howMany: number): Observable<Object> {
 		return this.http.get(environment.apiUrl + 'gettop/' + howMany,
 			{ 
				headers: new HttpHeaders().set('Authorization', 'Bearer ' + JSON.parse(localStorage.getItem('user'))['api_token']), // the headers
			});
	}

	login(email: string, password: string): Observable<any> {
		return this.http.post(environment.apiUrl + 'login', {
			email: email,
			password: password
		});
	}

	forgot(email: string): Observable<any> {
		return this.http.post(environment.apiUrl + 'userpasswordreset', {
			email: email
		});
	}

	reset(email: string, password: string, password_confirmation: string, token: string): Observable<any> {
		return this.http.post(environment.apiUrl + 'password/reset', {
			email: email,
			password: password,
			password_confirmation: password_confirmation,
			token: token
		});
	}

	getUserByRememberToken(token: string): Observable<any> {
		return this.http.post(environment.apiUrl + 'getuserbyremembertoken',
			{
				token: token
			});
	}

	logout(): void {
		this.http.post(environment.apiUrl + 'logout', {});
		localStorage.setItem('isLoggedIn', JSON.stringify(false));
		localStorage.setItem('user', JSON.stringify(null));
        this.router.navigate(['/']);
	}

	getMatchingHashtags(term: string): Observable<any> {
		return this.http.post(
			environment.apiUrl + 'matchinghashtags',
			{
				term: term && term != '' ? term : ''
			}
		);
	}

	deletePost(idPost: number): Observable<Object> {
		if (JSON.parse(localStorage.getItem('isLoggedIn'))) {
			return this.http.post(
				environment.apiUrl + 'deletepost', // the api url
				{id: idPost}, // the data to send 
				{ 
					headers: new HttpHeaders().set('Authorization', 'Bearer ' + JSON.parse(localStorage.getItem('user'))['api_token']), // the headers
				}
			);
		}
	}

	createPost(formModel: Detail) {
		if (JSON.parse(localStorage.getItem('user'))) {
			formModel.idUser = JSON.parse(localStorage.getItem('user'))['id'];
			return this.http.post<Detail>(
				environment.apiUrl + 'posts',
				formModel,
				{ 
					headers: new HttpHeaders().set('Authorization', 'Bearer ' + JSON.parse(localStorage.getItem('user'))['api_token']), // the headers
				}
			)
			.subscribe(data => {
		      this.router.navigate(['home']);
		    });
		} else {
        	this.router.navigate(['/']);
		}
	}

	updatePost(formModel: Detail) {
		if (JSON.parse(localStorage.getItem('user'))) {
			return this.http.post<Detail>(
				environment.apiUrl + 'updatepost', 
				formModel,
				{ 
					headers: new HttpHeaders().set('Authorization', 'Bearer ' + JSON.parse(localStorage.getItem('user'))['api_token']), // the headers
				}
			)
			.subscribe(data => {
		      this.router.navigate(['home']);
		    });
		} else {
        	this.router.navigate(['/']);
		}
	}

	private buildParamsUrl(searchParams: SearchPostsParams): string {
		if (!searchParams)
			searchParams = this._search.defaultSearchParams;
		let params: string = '';
		params += '/' + (searchParams.pageNumber ? searchParams.pageNumber.toString() : '0');
 		params += '/' + (searchParams.pageSize ? searchParams.pageSize.toString() : '50');
 		params += '/' + (searchParams.sortOrder ? searchParams.sortOrder : 'ASC');
 		params += '/' + (searchParams.filters && searchParams.filters != '' ? encodeURIComponent(searchParams.filters) : '_');
 		params += '/' + (searchParams.foodType ? JSON.stringify(searchParams.foodType) : '_');
		
 		return params;
	}
}
