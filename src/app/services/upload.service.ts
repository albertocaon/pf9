import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpEvent, HttpErrorResponse, HttpEventType } from  '@angular/common/http'; 
import { map } from  'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  	constructor(private httpClient: HttpClient) { }

  	newDishImageUploaded = new EventEmitter<any>();

  	upload(formData) {
		return this.httpClient.post<any>(
			environment.apiUrl + 'file', // the api url
			formData, // the data to send 
			{ 
				reportProgress: true,  
				observe: 'events',
				headers: new HttpHeaders()
				.set('Authorization', 'Bearer ' + JSON.parse(localStorage.getItem('user'))['api_token'])
				.set('Accept', 'application/json')
				.set('contentType', 'multipart/form-data'), // the headers
			}
		); 
	}
}
