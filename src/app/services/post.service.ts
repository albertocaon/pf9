import { Injectable } from '@angular/core';
import { Posts, Post } from '../interfaces/post';
import { SearchResult, Searches, SearchPostsParams } from '../interfaces/search-posts-params';
import { ApirezzemoloService } from '../services/apirezzemolo.service';
import { SearchService } from '../services/search.service';
import { FoodType, defaultFoodType } from '../interfaces/food-type';
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PostService {

	posts: Posts = {};
	searchResult: Post[] = [];
	postsPerRow: number = 1;

	constructor(private _apirezzemolo: ApirezzemoloService, private _search: SearchService) { }

	getPost(idPost: number): Observable<Post> {
    	const post$: Observable<Post> = new Observable((observer) => {
			if (this.posts[idPost]) {
				observer.next(this.posts[idPost]);
				observer.complete();
			} else {
				this._apirezzemolo.getPost(idPost).subscribe((post) => {
					this.posts[idPost] = post;
					observer.next(post);
					observer.complete();
				});
			}
    	});

    	return post$;
  	}

  	getPosts(searchPostsParams: SearchPostsParams): Observable<Post[]> {
  		searchPostsParams = this.onlyVegan(searchPostsParams);
  		const idSearch = this._search.getIdSearch(searchPostsParams);
  		const posts$: Observable<Post[]> = new Observable((observer) => {
			if (idSearch) {
				observer.next(this._search.searches[idSearch].result);
				observer.complete();
			} else {
				this._apirezzemolo.getPosts(searchPostsParams).subscribe((posts: Post[]) => {
					const newSearchResult: SearchResult = {
						searchPostsParams: JSON.parse(JSON.stringify(searchPostsParams)),
						result: posts
					}
					const newIdSearch: number = Date.now();
					this._search.searches[newIdSearch] = newSearchResult;
					observer.next(posts);
					observer.complete();
				});
			}
    	});

    	return posts$;
  	}

  	getPostsCount(searchPostsParams: SearchPostsParams): Observable<Object> {
  		searchPostsParams = this.onlyVegan(searchPostsParams);
  		const count$: Observable<Object> = new Observable((observer) => {
  			this._apirezzemolo.getPostsCount(searchPostsParams).subscribe((count) => {
	  			observer.next(count);
	  			observer.complete();
	  		});
  		});

  		return count$;
  	}

  	onlyVegan(searchPostsParams: SearchPostsParams): SearchPostsParams {
  		if (!searchPostsParams.foodType) {
  			this._search.searchPostsParams.foodType = defaultFoodType;
  		} else {
  			searchPostsParams.foodType.vegan = true;
  			searchPostsParams.foodType.veggie = true;
  			searchPostsParams.foodType.lactoseFree = true;
  		}
  		return searchPostsParams;
  	}
}
