import { Injectable } from '@angular/core';
import { Authors } from '../interfaces/user';
import { ApirezzemoloService } from './apirezzemolo.service';
import { User } from '../interfaces/user';
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UserService {

	authors: Authors = {};

	constructor(private _apirezzemolo: ApirezzemoloService) { }

	getAuthor(idUser: number): Observable<User> {
		const author$: Observable<User> = new Observable((observer) => {
			if (this.authors[idUser]) {
				observer.next(this.authors[idUser]);
				observer.complete();
			} else {
				this._apirezzemolo.getAuthor(idUser).subscribe((author) => {
					this.authors[idUser] = author;
					observer.next(author);
					observer.complete();
				});
			}
		});

		return author$;
	}

	isLoggedIn(): boolean {
		return JSON.parse(localStorage.getItem('isLoggedIn'));
	}

	logout() {
		this._apirezzemolo.logout();
	}
}
