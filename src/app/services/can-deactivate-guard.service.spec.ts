import { TestBed } from '@angular/core/testing';

import { CanDeactivateGuard } from './can-deactivate-guard.service';

describe('CanDeactivateGuard', () => {
  beforeEach(() => TestBed.configureTestingModule({
      	providers: [CanDeactivateGuard]
    }));

  it('should be created', () => {
    const service: CanDeactivateGuard = TestBed.get(CanDeactivateGuard);
    expect(service).toBeTruthy();
  });
});
