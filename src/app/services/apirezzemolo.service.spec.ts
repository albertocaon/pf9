import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { fakeAsync, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { ApirezzemoloService } from './apirezzemolo.service';
import { SearchService } from './search.service';
import { UserService } from './user.service';
import { environment } from '../../environments/environment';
import { dummyPosts } from '../mocks/posts.mock';
import { SearchPostsParams } from '../interfaces/search-posts-params';
import { Post } from '../interfaces/post';
import { Router } from '@angular/router';

describe('ApirezzemoloService', () => {
	let injector: TestBed;
	let service: ApirezzemoloService;
	let httpMock: HttpTestingController;
	const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [HttpClientTestingModule],
			providers: [
				ApirezzemoloService, 
				SearchService,
				UserService,
				{ provide: Router, useValue: routerSpy }
			]
		});
		injector = getTestBed();
		service = injector.get(ApirezzemoloService);
		httpMock = injector.get(HttpTestingController);
	});

	it('should be created', () => {
	const service: ApirezzemoloService = TestBed.get(ApirezzemoloService);
	expect(service).toBeTruthy();
	});

	it('should return an Observable <{pong: "pong"}>', () => {
	  const service: ApirezzemoloService = TestBed.get(ApirezzemoloService);
	  const dummyPong = [
	    { pong: 'pong' }
	  ];

	  service.ping().subscribe(pong => {
	    expect(pong).toEqual(dummyPong);
	  });

	  const req = httpMock.expectOne(`${environment.apiUrl}ping`);
	  expect(req.request.method).toBe("GET");
	  req.flush(dummyPong);
	});
});

