import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { UserService } from './user.service';
import { User } from '../interfaces/user';
import { Observable } from "rxjs";
import { ApirezzemoloService } from './apirezzemolo.service';
import { SearchService } from './search.service';
import { Router } from '@angular/router';

describe('UserService', () => {
	let userService: UserService;
	let injector: TestBed;
	const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

	beforeEach(() => {

		TestBed.configureTestingModule({
		imports: [HttpClientTestingModule],
			providers: [
				UserService,
				ApirezzemoloService, 
				SearchService,
				{ provide: Router, useValue: routerSpy }
			]
	})
		injector = getTestBed();
		userService = injector.get(UserService);	
	});

	it('should be created', () => {
	expect(userService).toBeTruthy();
	});

	it('should have authors object', () => {
		expect(userService.authors).toBeTruthy();
	});
});
