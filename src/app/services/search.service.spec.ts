import { TestBed, getTestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { SearchService } from './search.service';
import { Posts } from '../interfaces/post';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'

describe('SearchService', () => {
  	let injector: TestBed;
	const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
	let searchService: SearchService;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [HttpClientTestingModule],
			providers: [
				SearchService,
				{ provide: Router, useValue: routerSpy }
			]
		});

		injector = getTestBed();
		searchService = injector.get(SearchService);
	});

	it('should be created', () => {
		expect(searchService).toBeTruthy();
	});
});
