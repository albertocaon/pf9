import { Injectable, EventEmitter } from '@angular/core';
import { SearchPostsParams, Searches, SearchResult } from '../interfaces/search-posts-params';
import { FoodType, defaultFoodType } from '../interfaces/food-type';
import { Kinds, Kind, defaultKinds } from '../interfaces/kind';
import { Router } from '@angular/router';
import { Hashtag } from '../interfaces/hashtag';
import { GoogleAnalyticsService } from './google-analytics.service';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  searches: Searches = {};

	searchPostsParams: SearchPostsParams = {
		pageNumber: 0,
		pageSize: 12,
		sortOrder: 'ASC',
    foodType: defaultFoodType
	}

	defaultSearchParams: SearchPostsParams = {
		pageNumber: 0,
		pageSize: 12,
		sortOrder: 'ASC',
    foodType: defaultFoodType
	}

	searchPostsParamsChanged = new EventEmitter<boolean>();
	setDefaultKinds = new EventEmitter<boolean>();
  setEmptyFilter = new EventEmitter<boolean>();
  scrollPostStreamToTop = new EventEmitter<boolean>();

	selectedIndex: number = null;
	selectedPostId: number = null;

  hashtag: Hashtag = { name: '' };

  constructor(private router: Router, private _ga: GoogleAnalyticsService) { }

  reset() {
  	this.searchPostsParams = JSON.parse(JSON.stringify(this.defaultSearchParams));
  	this.router.navigate(['/home']);
  	this.setDefaultKinds.emit(true);
    this.setEmptyFilter.emit(true);
    this.scrollPostStreamToTop.emit(true);
  	this.searchPostsParamsChanged.emit(true);
    location.reload();
  }

  goToHashtag(hashtag: string) {
  	this.searchPostsParams = JSON.parse(JSON.stringify(this.defaultSearchParams));
    this._ga.eventEmitter(localStorage.getItem('isLoggedIn') ? 'admin-hashtag' : 'hashtag', 'browse', 'detailPostClick', 'goingToHashtag: ' + hashtag, 20);
  	this.router.navigate(['/hashtag/' + hashtag]);
  }

  getIdSearch(searchPostsParams: SearchPostsParams): number {
    for (let [key, value] of Object.entries(this.searches)) {
      if (JSON.stringify(value.searchPostsParams) === JSON.stringify(searchPostsParams)) {
        return parseInt(key);
      }
    }

    return null;
  }
}
