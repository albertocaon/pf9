import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { AuthGuard } from './auth-guard.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'

describe('AuthGuard', () => {
	const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
  	beforeEach(() => TestBed.configureTestingModule({
  		imports: [
  			HttpClientTestingModule
  		],
      	providers: [
      		AuthGuard,
      		{ provide: Router, useValue: routerSpy }
		]
    }));

  	it('should be created', () => {
		const service: AuthGuard = TestBed.get(AuthGuard);
		expect(service).toBeTruthy();
	});
});
