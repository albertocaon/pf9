import { TestBed, getTestBed } from '@angular/core/testing';
import { PostService } from './post.service';
import { SearchService } from './search.service';
import { Post } from '../interfaces/post';
import { Observable } from "rxjs";
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { Router } from '@angular/router';

describe('PostService', () => {
	let injector: TestBed;
	const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
	let postService: PostService;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [HttpClientTestingModule],
			providers: [
				PostService,
				SearchService,
				{ provide: Router, useValue: routerSpy }
			]
		});

		injector = getTestBed();
		postService = injector.get(PostService);
	});

	it('should be created', () => {
		expect(postService).toBeTruthy();
	});

	it('should have posts object', () => {
		expect(postService.posts).toBeTruthy();
	});	
});
