import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CustomService {

	bgColorEvent = new EventEmitter<string>();
	bgColor: string[] = ['black', 'white'];
	numberOfBgColors: number = 2;
	bgColorSelected: number = 0; 

	constructor() { }

	toggleBgColor() {
		this.bgColorSelected++;
		this.bgColorEvent.emit(this.bgColor[this.bgColorSelected%this.numberOfBgColors]);
	}
}
