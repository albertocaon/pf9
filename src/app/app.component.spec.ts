import { TestBed, async, fakeAsync, ComponentFixture, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { LogoComponent } from './components/logo/logo.component';
import { MenuComponent } from './components/menu/menu.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { KindSelectorComponent } from './components/utils/kind-selector/kind-selector.component';
import { FilterComponent } from './components/utils/filter/filter.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        MatTabsModule,
        MatMenuModule,
        MatInputModule,
        MatSelectModule,
        MatFormFieldModule, 
        FormsModule, 
        ReactiveFormsModule,
        BrowserAnimationsModule
      ],
      declarations: [
        AppComponent,
        LogoComponent,
        MenuComponent,
        KindSelectorComponent,
        FilterComponent
      ],
    }).compileComponents();
  }));

   beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'pf9'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('pf9');
  });

  it('footer click should call tracking function', fakeAsync( () => {
    fixture.detectChanges();
    spyOn(component, 'trackLcClick');
    const footer = fixture.debugElement.query(By.css('.footer'));
    footer.triggerEventHandler('click', null);
    tick();
    fixture.detectChanges();
    expect(component.trackLcClick).toHaveBeenCalled();
  }));
});
