import {
	trigger,
	transition,
	style,
	query,
	group,
	animateChild,
	animate,
	keyframes,
} from '@angular/animations';

export const slideInAnimation =
  trigger('routeAnimations', [
    transition('HomePage => DetailPage', [
      style({ position: 'relative' }),
      query(':enter, :leave', [
	    style({
	      position: 'absolute',
	      top: '0',
	      right: 0,
	      width: '100%',
	    })
	  ]),
	  query(':enter', [
	    style({ right: '-100%',  })
	  ]),
	  group([
	    query(':leave', [
	      animate('300ms ease', style({ right: '100%', }))
	    ]),
	    query(':enter', [
	      animate('300ms ease', style({ right: '0%'}))
	    ])
	  ]),
    ]),
    transition('DetailPage => HomePage', [
      style({ position: 'relative' }),
      query(':enter, :leave', [
	    style({
	      position: 'absolute',
	      top: '0',
	      left: 0,
	      width: '100%',
	      opacity: '1'
	    })
	  ]),
	  query(':enter', [
	    style({ right: '-100%', opacity: '0' })
	  ]),
	  group([
	    query(':leave', [
	      animate('300ms ease', style({ left: '100%', opacity: '0'}))
	    ]),
	    query(':enter', [
	      animate('300ms ease', style({ left: '0%', opacity: '1'}))
	    ])
	  ]),
  ])
]);

// cons = : true };
// let toTheRight = [
  // query(':enter, :leave', [
  //   style({
  //     position: 'absolute',
  //     top: '0',
  //     right: 0,
  //     width: '100%',
  //   })
  // ]),
  // query(':enter', [
  //   style({ right: '-100%',  })
  // ]),
  // group([
  //   query(':leave', [
  //     animate('300ms ease', style({ right: '100%', }))
  //   ]),
  //   query(':enter', [
  //     animate('300ms ease', style({ right: '0%'}))
  //   ])
  // ]),
// ];
// let toTheLeft = [
//   query(':enter, :leave', [
//     style({
//       position: 'absolute',
//       top: '0',
//       left: 0,
//       width: '100%',
//     })
//   ]),
//   query(':enter', [
//     style({ right: '-100%',  })
//   ]),
//   group([
//     query(':leave', [
//       animate('300ms ease', style({ left: '100%', }))
//     ]),
//     query(':enter', [
//       animate('300ms ease', style({ left: '0%'}))
//     ])
//   ]),
// ]

// // Positioned
// export const slider =
//   trigger('routeAnimations', [
//     transition('* => isLeft', toTheLeft),
//     transition('* => isRight', toTheRight),
//     transition('isRight => *', toTheLeft),
//     transition('isLeft => *', toTheRight),
//   ]);

// // Basic
// export const fader = 
// 	trigger('routeAnimations', [
// 		transition('* <=> *', [
// 			query(':enter, :leave', [
// 				style({
// 					position: 'absolute',
// 					left: 0,
// 					width: '100%',
// 					opacity: 0,
// 					transform: 'scale(0) translateY(100%)',
// 				}),
// 			], : true } ),
// 			query(':enter', [
// 				animate('600ms ease',
// 					style({ opacity: 1, transform: 'scale(1) translateY(0)' })
// 					),
// 			], : true } )
// 		]),
// 	]);