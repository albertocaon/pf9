import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { LOCALE_ID, NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ApirezzemoloService } from './services/apirezzemolo.service';
import { AuthGuard } from './services/auth-guard.service';
import { SearchService } from './services/search.service';
import { UserService } from './services/user.service';
import { CustomService } from './services/custom.service';
import { PwaService } from './services/pwa.service';
import { UploadService } from './services/upload.service';
import { HomeComponent } from './components/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PostComponent } from './components/home/post/post.component';
import { PostHeaderComponent } from './components/home/post/post-header/post-header.component';
import { PostThumbnailComponent } from './components/home/post/post-thumbnail/post-thumbnail.component';
import { PostFooterComponent } from './components/home/post/post-footer/post-footer.component';
import { KindSelectorComponent } from './components/utils/kind-selector/kind-selector.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import { PostStreamComponent } from './components/home/post-stream/post-stream.component';
import { FilterComponent } from './components/utils/filter/filter.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { DetailComponent } from './components/home/post/detail/detail.component';
import { RouterModule } from '@angular/router';
import { MatchUrlPipe } from './pipes/match-url.pipe';
import { MomentModule } from 'ngx-moment';
import { KindDescriptionComponent } from './components/home/post/detail/kind-description/kind-description.component';
import { IngredientsComponent } from './components/home/post/detail/ingredients/ingredients.component';
import { RecipeComponent } from './components/home/post/detail/recipe/recipe.component';
import { DateComponent } from './components/home/post/detail/date/date.component';
import { ViewsComponent } from './components/home/post/detail/views/views.component';
import { MenuComponent } from './components/menu/menu.component';
import { LogoComponent } from './components/logo/logo.component';
import { HashtagsComponent } from './components/hashtags/hashtags.component';
import { LoginComponent } from './auth/login/login.component';
import { AuthComponent } from './auth/auth.component';
import { ForgotComponent } from './auth/forgot/forgot.component';
import { ResetComponent } from './auth/reset/reset.component';
import { LoginRoutingModule } from './login-routing/login-routing.module';
import { SettingsComponent } from './auth/settings/settings.component';
import { DishComponent } from './auth/dish/dish.component';
import { UploadImageComponent } from './auth/upload-image/upload-image.component';
import { AdminControlsComponent } from './components/home/post/detail/admin-controls/admin-controls.component';
import { PostChunkComponent } from './components/home/post-stream/post-chunk/post-chunk.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { TopComponent } from './components/home/top/top.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { ShareIconsModule } from 'ngx-sharebuttons/icons';
import { GoogleAnalyticsService } from './services/google-analytics.service';
import { InfoComponent } from './components/info/info.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PostComponent,
    PostHeaderComponent,
    PostThumbnailComponent,
    PostFooterComponent,
    KindSelectorComponent,
    PostStreamComponent,
    FilterComponent,
    DetailComponent,
    MatchUrlPipe,
    KindDescriptionComponent,
    IngredientsComponent,
    RecipeComponent,
    DateComponent,
    ViewsComponent,
    MenuComponent,
    LogoComponent,
    HashtagsComponent,
    AuthComponent,
    LoginComponent,
    ForgotComponent,
    ResetComponent,
    SettingsComponent,
    DishComponent,
    UploadImageComponent,
    AdminControlsComponent,
    PostChunkComponent,
    TopComponent,
    PageNotFoundComponent,
    InfoComponent,
  ],
  imports: [    
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatSelectModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatTabsModule,
    MatMenuModule,
    MatCardModule,
    MatProgressBarModule,
    MatIconModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatProgressSpinnerModule,
    ScrollingModule,
    RouterModule,
    MomentModule,
    LoginRoutingModule,
    ShareButtonsModule.withConfig({
      debug: false
    }),
    ShareIconsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [
    AuthGuard,
      ApirezzemoloService,
      SearchService,
    UserService,
    CustomService,
    UploadService,
    PwaService,
    GoogleAnalyticsService,
    { provide: LOCALE_ID, useValue: 'es' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
